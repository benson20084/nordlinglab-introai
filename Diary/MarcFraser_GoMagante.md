This diary file is written by Marc Fraser Go Magante F74077120 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-21 #

* I've watched the welcome video when it first went up and I am very excited about what the course has to offer.
* I just did the successful story activity and I've found out that A.I. has a lot of applications in different kinds of fields.
* I am really excited to learn how to program using Python.
* I was actually very confused if what we were supposed to do at first until I saw TA's notes on moodle, shout out to the TA bro you did a great job.
* This is my first day using bitbucket and my first day doing the diary.

# 2020-03-28 #

* I've read about What is AI?
* So in the first part, they introduced some applications of AI like self-driving cars, content recommendation, image and video processing.
* In self-driving cars, we need computer vision, search and planning, and decision making to work with almost flawless precision in order to avoid accidents.
* There is no exact definition of AI because it is constantly changed when there are new topics and some topics will become non-AI. 
* The meaning of AI is usually being confused due to cinematic and literary works which depicts AI as something related to humanoid or humanlike creatures.
* There are some things that we find easy and AI finds it difficult to do and vice versa such as picking up or holding an object since they don't know how much force they need to grasp the object
  since an object has sizes and sometimes the weight varies.
* For the vice versa, AI has the advantage when it comes to playing chess and solving mathematical exercises since AI will compute many alternative move sequences at a rate of billions of computations a second
  which makes them have the upper hand.
* The key characteristics of AI are autonomy and adaptivity which means an ability to perform tasks in complex environments without constant guidance by a user and the ability to improve performance by learning from 
  experience.
* So in AI, there are some topics that are related to it which are machine learning, deep learning, and data science.
* Machine learning is a subfield of AI. The role of ML is to let the systems improve their performance in a given task with more and more experience or data which enables AI solutions that are adaptive.
* Deep learning refers to the complexity of a mathematical model. With the increased computing power of modern computers, it has allowed researchers to increase this complexity to reach levels that appear not only 
  quantitatively but also qualitatively different from before.
* In the philosophy of AI, I find the topics interesting like the turing test, Eugene Goostman, Chinese room argument.
* In the Turing Test, I find it very interesting because it's like a guessing game between determining a computer and a human and to determine if the robot can message like a human. 
* And about Eugene Goostman, it was very cool to see a computer message like that because it looked it was a human that was messaging jokingly.
* The Chinese room argument suggests that self-driving cars doesn't have intelligent thinking because the AI system in the car doesn’t see or understand its environment, and it doesn’t know how to drive safely, 
  in the way a human being sees, understands, and knows.
* With Searle's ideology, strong AI would amount to a “mind” that is genuinely intelligent and self-conscious. Weak AI is namely systems that exhibit intelligent behaviors despite being “mere“ computers.

# 2020-04-05 #

* I am actually not sure what is the topic for this week so I just looked up regarding the supposedly 3rd week of the class' lecture which is about Python's comments, Printing, Conditional statements and loops.
* I learned some of my Python's functionality like comments, Printing, Conditional statements and loops.
* It is actually not that hard as I thought it would be.
* I am a CSIE student so learning Python is a huge plus for me.
* I use Sublime as my text editor for programming Python.
* I just realized that Python doesn't really use semicolon and curly braces that much compared to other programming languages.
* Python uses colon (":") for statements which is curly braces ("{}") for other programming language.
* I find the "sep" and "end" keyword arguments for the print function to be very cool.

# 2020-04-12 #

* This week I think we are learning about Python's Variables and functions, Reading and writing files, and Error handling.
* I am a CSIE student so learning these in Python is quite familiar to me.
* I just realized that when declaring variables in Python, it is not necessary to write what data type our variable is before declaring it compared to other programming languages.
* In Python, what ever you put either string, int, or anything, python already can define it what data type.
* Specifying the data type is also allowed in Python.
* In Python, we need to define the function first so we could write a function through "def" then function name.
* In a function, you could put parameters or arguments inside it and it would be used inside the function.
* When reading and writing a file, we need to open the file first through open().
* When reading a file, we can use read(), readline(), and readlines().
* When writing a file, we can use write() and writelines().
* For error handling, syntax errors are the most basic and common type of error and arise when the Python parser can’t read a line of code. These errors typically cause the program to crash. 
* For logical errors, the program doesn't crash but it produces unpredictable results.
* Sometimes even if a statement is syntactically correct, an error may arise when executing it. These errors are referred to as exceptions.

# 2020-04-19 #

* This week was our first online classs and it was all about the introduction of the course and the professor.
* I've went through the about the basics of Python programming and Tensorflow via Google Collab.
* I also watched the video regarding Deep Learning in Youtube.
* Neural Network is actually quite complicated than I might have expected it to be.
* It needs a lot of data to somehow train and to determine the pattern of the image.
* The Activation is the number from 0 until 1 depends on how black or white the image is.
* There are layers to determine the specific number in the pixel through finding the right weights and biases.
* You can input a bias number into the sigmoid function to calculate activation value into the next layer of the network.
* At the end of the video, it tells us that Sigmoid is a slow learner compared to the new RELU.
* The no free lunch theorem shows that the more the training sets, it will lead to lesser test errors as there are fewer incorrect hypotheses.
* There is unsupervised learning algorithm and supervised learning algorithm that associates examples with label or target.

# 2020-04-26 #

* I watched a video about the introduction to recognition of handwritten digits using the MNIST example through "TensorFlow and Deep Learning without a PhD" in youtube.
* It is very complicated since you need to be familiar with Tensorflow or its library which contains a lot of features for machine learning.
* Even if it is complicated, learning Tensorflow is very useful in the long run.
* Tensorflow manipulates data by creating a DataFlow graph or a Computational graph. With that, it is now being widely used to build complicated Deep Learning models.
* I will try to do some practical stuff using Tensorflow if I have time.

# 2020-05-03 #

* Started to use the Google Colab and learned how Jupyter Notebooks work and how to run python code with them
* Did some practice in Collaboratory.
* I am quite busy this week due to exams so I don't really have time to try out Tensorflow that much.
* I also learned some Python in Code Academy to have some idea about the syntax.

# 2020-05-10 #

* This week the professor announced the groups for our project in this course.
* I watched the video about Gradient Descent.
* It is a function to optimize and minimize the cost of the model.
* I also watched the video about Back Propagation.
* It is used to feed forward neural networks in order to fine tune the weights of the network through minimizing the loss or error rate.

# 2020-05-17 #
* This week, the professor talked about the group project, the possibility of having the class in school, and the difference of A.I., Machine learning, Deep learning and more.
* I found my group members during the online class.
* Our group member created the group chat for the group project and also he created a poll to know where we will discuss about the project.
* I watched "Fake videos of real people-and how to stop them". 
* "Reality defender" is needed at this rate for computer development.
* Deep fake video is frightening since its potential can be misused.
* A possible application for its misuse would be politics.

# 2020-05-24 #
* This week, we had our first physical class and it was fun.
* We had a discussion about ethics and the danger of algorithm bias. It was really fun since we get to meet new people and discuss our opinions about it.
* For now, I think it isn't possible to make AGI pssible. Since it has to learn how our society behaves but our society doesn't treat every people equally since there is still discrimination.
* It will see that people just hate each other and it will assume that it is normal.
* I also was able to meet my groupmates and I am glad we were complete. We will be having our first meeting on Wednesday for the project.

# 2020-05-31 #
* This week, we didn't have a class but instead we had a group discussion regarding our group project.
* We discussed about the code and how it works.
* We also learned new things along the way like some terms we never heard of.
* We will be having another group meeting around next week to finalize our work and make a video about it. 

# 2020-06-07 #
* The class was interesting yet scary at the same time.
* It was about how automation will affect future jobs which is why it is scary.
* In the near future, as machines and robots keep getting advance and better, a huge percentage of the world’s current jobs can be replaced by them. 
* In the future, my job is still secured since my major is computer science and in professor's discussion my job was not mentioned.
* He also talked about electric cars and gasoline cars and that in the future, electric cars will be cheaper than gasoline car. Therefore, more people will buy electric cars in the future. 

# 2020-06-14 #
* I was able to meet with my groupmates for the group projetct.
* We finished making a video about the project.
* Currently busy studying for the final exam.
* I can't wait for this semester to end. :)