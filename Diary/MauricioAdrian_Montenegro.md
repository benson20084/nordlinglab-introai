Lecture 1
I had a hard time getting used to using bitbucket, but i guess little by little i will be getting used to it*
Before taking part in this class i had very little knowledge about AI, mostly what i knew about it came from Isaac Asimov's books like I robot or the last question*
I didnt really have concrete definition of what AI is and after reading the study material i have finally understood*
I hope i can expand my understanding of AI with this course*

Lecture 2

How should we define AI?
There is not an exact definition of what AI is, not only from the material i have read but also renowned physicist Michio Kaku agrees with this*
AI should not be conceived as a thing but rather as a discipline like Math or Biology*
Autonomy and Adaptivity are characteristics of AI*

Related Fields

Machine learning are systems that improve their performance in a given task with more and more experience or data*
Robotics is the greatest challenge within AI, 2 questions come up to my mind about this, will robots represent a threat to humans in the future?*
From a philosophical perspective, will this make us some kind of God? *

Philosophy of AI

Although i remember reading about Alan Turing back when i was in high school, i had never read about the turing test, which in my opinion is very interesting*
Even though i do not dare to criticize Turings Test, i believe there is one particular flaw in it, which is that it's limited by the individual's intelligence*
The chinese argument seems to me pretty clever*

Lecture 3

Search and problem solving

The Chicken crossing puzzle is something i had heard before, when i was a kid and reading it once again seems to remain the essence and quite interesting
but never had thought of assigning states to each of the possible ways to solve it and not only solving it but being able to do it in a more efficient way*

Solving problems with AI

The term Artificial Intelligence was coined by John McCarthy (1927-2011) – who is often also referred to as the Father of AI*

Search and Games

I understood the concept behind game trees, and it seems quite useful for easy games like tic tac toe, but not for more complex ones like chess*
Assigning values to chess pieces is quite intuitive, but also assigning value to some squares which are more important than others*

2020/03/29----2020/04/05

No Assignments for 2 weeks

2020/04//18
First synchronous class

2020/04/26
Second synchronous class

2020/05/13
Looking forward to see if next weeks class will be online or face to face, also looking forward to talk about ethics in artificial intelligence*

2020/05/20
Our first in-class discussion about ethics, advancement of AI was quite interesting*
Different opinions while in class is quite enriching as well as eye opening*
I believe the professor's final statement was absolutely correct when he said, in order for AI not to be a threat to us, we should first learn how to live peacefully, in harmony*

2020/05/27
I have been working on my project along with my partner, have been learning quite a lot, but don't seem to enjoy programming too much*

2020/06/03
Have been working on my project along with my partner and we have made our number recognition code have an accuracy of 99%*

2020/06/10
Finished our project, just need to record the video*