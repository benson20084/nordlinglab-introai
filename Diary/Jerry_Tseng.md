
This diary file is written by jerry tseng E84056011 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-29 #

* know how to edit diary.

# 2020-04-02 #

* I watched the AI article.
* See the question posted by the classmate.

# 2020-04-12 #

* Watch 3blue1brown.
* Read Ch. 5.1-5.3 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning.

# 2020-04-19 #

* Watch the following parts of the tutorial "TensorFlow and Deep Learning without a PhD" by Martin G�rner.

# 2020-05-03 #

* Learn to code.
* Introduction to Python.
* Watch how Convolutional Neural Networks work.

# 2020-05-10 #

* Watch Ethics and the danger of algorithm bias.

# 2020-05-17 #

* Watch AI "Stop Button" Problem - Computerphile.