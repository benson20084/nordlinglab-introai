This diary file is written by Zhe Hao Lin B34051246 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-03-11 #

* The first lecture was excited.
* I watched introducing video of AI.
* I read some news about Amazon Go and wrote first homework about it

# 2019-03-25 #
* I subscribed the tech news web-Techcrunch to learn more about technology.
* Hope I could learn more from this class.

# 2019-04-15 #

* Fineshed (Getting Started with Python) on Coursera and practice Python.
* I read tech news once a day

# 2019-04-22 #

* I started my Python Data Structure courses on Coursera, things getting hard to learn.
* I tried to figure out how Google Colab works.
* Reading list : Basics of tensor flow, MNIST dat sets
* I excited about how would my module at the end of the semester work.

# 2019-04-29 #

* I watched a video of an artificial neural network, it provides a clear campaign of machine learning.
* learn the difference between unsupervised and supervised algorithm.
* Actually, it taked me 3 hours to figure out.

# 2019-05-07 #

* Reading list : Data ETL(Extract, Transfer, Load), Data Cleansing, Data Mining.
* Try to run TensorFlow on my computer, but with some technical problems it take me several hours to work.
* Thanks to my friends, I discuss with them about my problems and Python.

# 2019-05-13 #
* Get started learning Python Chapter Dictionaries
* Discussed group project with my team memebers.
* Read the news "The New Business of AI" 

# 2019-05-20 #
* Is AI always right? How could we prevent the mistakes & bias from AI?
* Discussed with group memebers.
* Get started learning Python Chapter Tuples

# 2019-05-27 #
* Discussed group project with my team memebers, and things going on.
* Finished my Python course on Coursera and got certificate.
