This diary file is written by Manuel Carrera F04057134 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-22 #

* Today I created my Diary file.
* After reviewing the first lesson slides, I have a better understanding on the applications of Artificial Intelligence.
* The real life applications are interesting, they are all algorithms that are able to find patterns and execute actions in the best way possible.
* I found interesting the fact that in all of these applications, like AI for chess or AI for image recognition, the algorithm is able to outperform humans.

# 2020-03-29 #
* This week I started to understand more about what AI really means.
* First, it's important to keep in mind some of the misconceptions regarding AI. AI is not a superhuman concius intelligence the way pop culture makes it look like.
* Artificial Intelligence is actually a dicipline of computer sience which is based on algorithms that deal with autonomy and adaptivity.
* Autonomy means that such computer algorithms , once implemented, could work by themselves without an active intervention of the user.
* Adativity is the capacity of these algorithms to take the data from the previous run, try to make a conclusion out of it and "learn" from it for future runs.
* Artificial Intelligence actually has to do more with data analysis than with our human perception of "organic" intelligence. And yes, with a proper algorithm, a computer can execute this tasks more efficiently.

# 2020-04-05 #
* Since we started spring break this week, I was able to review a bit of the material, which is Python.
* Python is a programing language that is going to be the main tool for our class.
* I am also aware that Python is a high level promraming language and so it's been realatvely simple to follow the basic instructions so far.
* I have been watching videos on YouTube on how to get started in Python. I like to watch videos instead of reading tutorials, I find myself in a better learning experience, although I know this only works for the basics.
* My background of programing in C languge is coming in hand, especially since Python is a bit more intuitive. 

# 2020-04-12 #
* This week I focused on reviewing the basics of Python as well as understanding the recommended coding skills listed on the course content layout.
* For anyone reading this diary, when it comes to variables and functions I really recommend this Youtube video. In the video they introduce the concepts of variables, functions and how they relate to each other, however if you are completely new to programing this video may get a bit confusing towards the end, but for someone that has some experience programing, this video is really useful: https://www.youtube.com/watch?v=IEel7loz-ag&list=PLYlZ5VtcfgitfPyMGkZsYkhLm-eOZeQpY&index=2 
* I also found another video in Youtube that includes a total of 4.5 hours worth of introduction to Python content. It's a really good resource to review all the basics and this particular video is really beginner friendly, so I will also recommend to check it out: https://www.youtube.com/watch?v=rfscVS0vtbw .
* After reviewing these video lessons I feel a bit more comfortable managing variables, functions, boolean variables for logic loops, debugging etc.
* An interesting feature of Python that I learned was the ability of reading and writing files, I feel is a really useful tool and I feel Python makes it not so hard to implement.

# 2020-04-19 #
* This week I learned more about how mchine works in general especially how neaural networks.
* Neural Networks are a tool for machine learning that focuses on recognizing patters, in a somewhat similar way humans do.
* The name Neural Networks as it implies, uses a network of neurons, each neuron is a value in the algorithm that could take a value from 0.0 to 1.0, and each value represents if the variable is "off" at 0.0 or "on" at 1, or anything in between.
* The point of Neural networks is to use the values of the neurons and assign a "weight" to them in order to assign which neurons are more important for our application.
* The next step is to assign different "layers" that process the results of the neurons before it, that way we could proceess or "filter" the information until we can find a patttern of the input information and hopefully get the necessary results.
* The topic of Neural Networks seems really interesting to me so I hope we can learn and practice more about it in this class.

# 2020-05-03 #
* This week we had our third online class. It was mostly compound of Q&A sections where the professor took the students questions and answered them, it was useful to review some details of the deadlines and tasks aswell.
* I have been focusing on unerstanding the works of a neural network. I think this topic seems a bit challenging at the begining but is still important to try our best to understand how neural networks work before we have hands-on practice, however I also believe that by having hands-on experience we could have a better learning process.
* This week I also started learning about the Tensor Flow toolkit and its implementation with Python. I feel greatful that such tools exists so that a real project can be realistically doable in this introductionary course.
* There is a great source online which is the Google codelabs guidelines. I have read the begining of it and I still have a great way to go but so far seems well explained and I hope I can learn everything it's trying to teach. Link: https://codelabs.developers.google.com/codelabs/cloud-tensorflow-mnist/#0 .
* We are on the last steps of the midterms period of this semester. After this week I hope I can find more time to get more involve with this Tensor Flow material so that I can start experimenting and run the codes in my computer and start to get ready for the final project of this signaturte.

# 2020-05-10 #
* This week I keep learning on how neural networks work and how they execute their tasks, in other words, how neural networks "learn".
* First it is important to understand that the way neural networks work is by assigning a "weight" to all the variables that are the initial input data as well as to the following layers of processed data, the weight basically describes the "importance" or "relevance" of that input data to the next set of processed data.
* Neural networks need to "train" at the begining. The way they do that is by inputing initial data as well as giving it's desired output value. Then we need to compare the actual result to the desired result, after that we asign a number which represents how good this output was. This number is given by the cost function, and if it is a high number it means that the neural network performed poorly and if the cost function's value is low it means the algorithm showed a good result.
* The way we can make the value of the cost function to decrease is by changing and "tweaking" the weights of all the variables in the neural network. The way for us to know if we need to increase or decrease the value of each weight and by how much is by means of a partdial derivative of all the variables.
* This partial derivation of all the variables is called "The gradient", the gradient tells us which way we should go in order to find the "minimum" of the cost function by interpretation of the "slope" of the function, in other words the gradient gives us a function that by following in its negative direction will lead us to how much the values of the weights should change so that the cost function approaches zero, which in turn will represet a better perfomance by the neural network.
* An interesting thing to notice is that this so called "Gradient decent" is basically the way the neural network "learns".
* The second important concept that I learned this week is called backpropagation which is how the gradient decent is actually applied in the algorithm.
* First we need to understand that from the input data to the output result there are many different layers of neurons through which the data gets processed.
* By applying gradient decent from the output information we will get the values of how to change the weights to each neuron on the layer before that. Then after we have that value then we can take the gradient decent agian for that layer of neurons in order to know which weights to chanche for the layer before and so on and so on.
* So "backpropagation" is basically taking the gradient decent one time for each neuron layer of the neural network in order to eventually come with the way we should change all the weights in the whole neural network in orther to make the cost function to approach zero.
* An interesting note is that ideally we will do this process for every input sample we have in the training process, however this will be a slow task, so in practice, there are different techniques to do this task faster, although not necessarily showing good perfomance.

# 2020-05-17 #
* Continuing from last week, I finished watching the video series of neural networks from 3 brown 1 blue youtube channel.
* I concentrated on understanding the calculations behind the concepts of gradient decent and backpropagation that I reviewed last week.
* Basically what we need is a way to understand how much a wight affects the end result of the cost function since we wish to decrease it as much as possible. The way we can understand how much a weight of a neuron affects the cost function is by taking its ratio.
* The ratio between the cost function over the weight of an especific neuron which at the same tame is represented by the ratios between the variables that affects the weight itself.
* The weight of each neuron is influenced by the activation value of the neurons of previous layes, their weights and a bias function. If we evaluate the ratio between this variables, the result will be the ratio of the cost function to this especific weight.
* We remember from calculus that a ratio is actually a derivative of a function with respect of the variable we wish to know the ratio of. So all of this boils down to executing many partial derivatives to eventually understand the influence of all the connected neurons and weigts so that we can have a good chance to succesfully change them and improve the network.
* Another important lesson that I learned this week is that in practice, it turns impractical to tweak the values of all these weights for every training sample.
* What is realistically doable is to execute the partial derivatives and take the average of these values before we actually change the weights. That way we can improve the network in a shorter amount of time, although we will be sacrificing the precision of the adjustments. However we can still expect the neural network to eventually become good enough. 

# 2020-05-24 #
* This week we had the first precensial lecture since the coronavirus situation is well undercontrol in Taiwan so far.
* First we discussed about the Ethics of artificial intelligence and how it affects the development and the applications of these types of algorithms.
* The main topics that we discussed are the possible applications, people and companies can do with artificial intelligence.
* Applications that threatens the personal information of social media users as well as things and decisions were there is a general consensus that it's better to be carry on by a human.
* Those applications ar such as law and criminal judgement as well as medicine and ilness diagnosis. 
* We also discussed other not so obvious topics, such as AI for nutrition and diet recommendations, as well as AI for grading and academic performance. On this two topics is not too clear whether an AI algorithm is good, or a human would be more suitable for the task.
* Another interesting idea we talked about was about an Artificial General Intelligence (AGI), which is an AI that is able to learn by itself new things, as well as beign capable to train itself to learn those things without human intervention. I personally believe that such thing is not possible, at least an artificial intelligence that is as organic and general as ours, I don't think so.
* This week I also had the opportunity to meet with my teammates for the final project, we arranged some things and we will meet next week to start working on the final project.

# 2020-05-31 #
* We are pretty much entering the final stage of this semester, so now it's the time to start working in the final project.
* On this week me and my team meet up in the library so we can start working on the final project. At the begining we had a brainstorm and dicussed how we wanted to achieved the final project and we disected it into differemt task. 1.- Get the code. 2.- Understand the code. 3.-Run the code (troubleshoot). 4.-Train the neural network and analyse results.
* After searching for a while, we encounter a good source that explains the critical parts of the code and how it comes into place when we are executing the artificial intelligence to recognize handwritten numbers.
* We are also using google's colabs so that we can edit the code online. We are also using the advantage of the google servers so that we don't need to install a compiler and the libraries into our computers.
* So far the final project seems to be running smoothly and I hope we can run the code sucessfully so that we can start working on the final presentation / video. Which also needs to be agreed upon.

# 2020-06-07 #
* This week he had our second in person class. The topic of this week was aoutomation and the role Artificial Intelligence is playing to achived great levels of automation in industry.
* First we talked about the probelms that the earth is facing right now, such as pollution and missuse of fossil fuel. These problems can be improved with an energy efficient economy, and AI makes it more feasible.
* However we also talked about the jobs that can be easily replace by an AI. So in my opinion, the jobs that are the most likely to be replace by machines are those who fall into the category of being "mechanical" jobs. What I mean like that is that, there are some jobes that doesn't require a high level of human cognitive performance in order to be executed. These kind of jobs are the ones that can be easily replaced.
* At the same time we need to be concerned with the ethic of taking a person's job for the seak of optimization and automation. What would happen to all of those people if they suddenly lose their job? What are their options?. These questions are not only interesting but necessary. Sadly they don't have a concrete answer.
* Regarding the final project. My team and I talked and worked on the final details on testing and training our artificial neural network. We had a great progress and I think we are going to be ready for the final presentation day.
