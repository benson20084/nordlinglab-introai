This diary file is written by 王子瑜 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* Watch the welcome video.
* Read success stories of AI.

# 2020-03-25 #

* Finally learnt how to write a diary.

# 2020-04-01 #

* Read the website "Elements of AI". 
* Application of AI: self-driving cars, algorithms of content providing, 

# 2020-04-08 #

* Watch the video "AlphaGo"
* Read the website "Elements of AI"

# 2020-04-15 #

* Attend the synchronous online lecture

# 2020-04-22 #

* Attend the synchronous online lecture

# 2020-04-29 #

* (I had a ferver and a really bad headache, so I didn't attend the synchronous online lecture)
* Read the materials for coading in python. Although I have learnt a little bit about python, this material still reminds me of something I didn't noticed before.
* Still finding some suitable materials for the success-story report......(Sorry teacher, I will try my best to finish it ASAP)

# 2020-05-06 #

* learn more about coding in python