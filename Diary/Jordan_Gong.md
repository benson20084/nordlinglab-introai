This is a diary file written by Jordan F44044066 to the course of AI introduction.

# 2019-02-20
* get some success stories of AI application. Go game, lip reading, image caption, skin cancer and diabetic retinopathy
* I found it super hard to do lip reading. How amazing that AI nail it with such a high accuracy (95%)

# 2019-02-27
* A great description to distinguish statistics, data science, data minging, system id, deep learning and AI
* Wondering what contributes to the boom of AI in recent years. The main reason I come up is the development of hardware and Internet speed.
* My own definition of AI: "an automatic aiding tool based on computer algorithm which helps human realize something impossible"

# 2019-03-06
* Based on my knowledge about C++, python seems to have less limit on the data type, surprising!
* About the class and inherence, it is a little bit awkard, for example the "self" variable in the init. function.
* Also, I am curious about what is the language that major AI commuity uses. Later, I find that python is the most widely used language because of its simplicity and useful packages, for example: Numpy, Scypy and Pybrain.

# 2019-03-13
* play with python. Notice the data type called 'tuple', which is strange at the first glance
* tuple is a data type using () to declare. It cannot be modified after created. It is immutable.
* While on the other hand, list, another data type is declared with []. It is mutable. Very similar to 'array' in C++

# 2019-03-20
* Very fascinated about the idea that neural network network is nothing but a function aproximator. Limpid description!
* This idea connects me with the renowned "engineering tool" Fourier seires, which uses a group of sin and cos functions to approixmate any continuous function. Quite amazing, right?
* What also astonishes me is that in Fourier series, there is a systematic way to find the exact coefficient value of those group functions, maybe there is also a way in neural network!??
* I think we, humankind do well on reinforced trainning because there exists a definite answer.
* Thus, it drives me a lot, since so much AI potential are still waiting for us to explore, an uncharted terriotry!!!

# 2019-03-27
* Notice the difference between classification and regression
* Quite amazing at the fact that more training data will eventually lead to more error. I believe this explains the situation I faced with my experiment~~~
* Engineers strive for "USEFUL MODEL", instead of "real, ultra-true model". I agree with this concept.
* Wondering how can we calculate the gradiant when we implement backward propogation method to train neural network?

# 2019-04-10
* As we do the gradiant descend method, the step is is critical. How to choose it is an art.
* Convolutional Neural Network is typically used for image recognition. The core idea here is to use filters to "capture the features" of the images before transferring to the neural networks, otherwise it might be too complicated. I like this laconic description that: CNN is a "sparsely connected" network.
* Quite impressing about how CNN similar to human behavior of recongizing images. Yes, we tend to extract the useful part of the images before we interpret them. In other words, we sift the image subconsciously. 

# 2019-04-17
* To implement deep learning, tensorflow is an suitable package to use in python environment.
* The "hellow world" program for AI is number recognition from 0 to 9. A well-developed dataset for this task called MNIST is imported.
* I am quite intereseted in how to post the program to the internet since it would be cool to share my work with others.

# 2019-04-24
* (Solved) First problem for the Number Recognition program: loading mnist data. I found that it cannot be run on python idle shell. Instead, to successfully load the mnist data set, I use command window to launch the program.
* (Solved) Second problem. In order to train the model, an additional package : scipy must be installed. This is not included in the tutorial guide.
* Third one. I am now struggling with the image.load_img function, it keeps sending error. I wish I can fix it.
* Finally, the third problem was solved. To load the image, another addition package should also be installed by running the code in cmd: pip install pillow.
* In addition, I modify the code in app.py so that now it can showcase two prediction from two different AI models. 1: one hidden layer 512 nodes. 2: two hidden layers with 20 nodes each.
* So amazing to watch "them" predict digits haha~

# 2019-05-01
* After the pratice of digit recognition, I feel that deep learning is not that timidating as before. It is a good sign!
* Since I am now learning about algorithm and data structure, I am quite curious about how we can integrate this two parts together and make a better AI.
<<<<<<< HEAD

# 2019-05-15
* Interesting lecture. I feel like the trend of AI is similar to the issue of climate change, in which both are too huge and complex that most of us choose to ignore until the sounded data and evidence are provided.
* So, I have a question: What are the works people in Norway do after most of the jobs are automated? Driven by the curiosity, I vistied an authority website called Statistics Norway. From their data, the top five popular works are: Human health and social work activities, Wholesail and retail trade: repair of motor vehicles and motorcycles, Construction, Education and Manufacture.
* I cannot agree more with the claim that creativity is the most essential part for our future career. By some discussion, I come up with a conclusion that in engineering field, creativity means keep exploring new things, it means to discover uncharted territory. For example, the gravitational wave experiment, the seek for the image of black hole, the examination of quatam physics and so on. It also reflects the ability of exploiting our imagination.
* For the third industry revolution topic. I have a lingering question. What will power the commercial aircraft, which carries hundreds of people over continents, in the next generation if we are certain to switch from fossil fuel to renewable energy? 

# 2019-05-22
* One classmate shared an interesting story that Xiou Mi smart watch cannot detect the heart rate of his black classmates. It is an excellent an real example of algorithm bias.
* Also, we discussed about the bias of google searching engine as well as the recommendation algorithm of youtube. And I feel that youtube really narrow how we think, and what we percieve.
* The last footage we watched is about fake face AI. I knew this before because of some "special occasions." But it was up to this morning that I realized the initial purpose of the developer of this tech. is nothing but positive and encouraging rather than evil or triggering chaos. However, I really want to point out that it is a typical trend of advancement for every technology. The recent and classic example is atomic bomb. Most technology is born for good. And it is our duty to guide them in a positive direction. 

# 2019-05-29
* We discussed about the architecture DNN. I found 2 definition: 1. Deep neural network 2. Deconvolutional neural network
* I build the first model based on CNN neural network. The accuracy of the testing data now is 99.2 %.
* An interesting part is that in the code, the author use a random function, which I still do not know how it works.
* Another interesting part is about voting algorithm.

# 2019-06-12
* Really excited to see how our classmates build their Mnist model.
* I was really obsessed with buiding an innovative model, and see how well (or even how bad) it works haha.
* I made a video about how to automatically save the best epoch mode during training. I'd like to share with the classmates~
* Also, building an AI indeed requires teamwork. During our development, I was once stuck and could not make any breakthrough. However, with the idea and inspiration of our teammates, we finally built some really interesting architecture!
* In total, we have 4 different architechture. And the best architecture val_error is 0.38 % recently.

# 2019-06-19
* Fascinated by others' group projects.
* Some of the groups tried English letter recognition, in which they use EMNIST data.
* Glad to be in this course for my last semester in college.
