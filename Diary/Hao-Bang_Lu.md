This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Ha-Bang, Lu C24031211 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* I'm not used to online courses. I thought there would be a video lesson or a videocall. I was panic because I don't know where to access.
* I heard about Python before and I started to learn about web application since this Tuesday. I watched some videos throughout the week to [use Django on repl.it](https://www.youtube.com/watch?v=sM1xFEZ7-q0).
* I read through all the ppt in the [google drive](https://drive.google.com/drive/folders/1DvUkAn9Z-77pKSa6ONJYHVoJ1SQgBsFL). My favorite AI success stories are about [dressing mirror](https://drive.google.com/drive/folders/1DvUkAn9Z-77pKSa6ONJYHVoJ1SQgBsFL) and [web design](https://drive.google.com/drive/search?q=Website%20Design%20Modifications%20with%20AI).
* I think AI is amazing. I'm looking forward to [intuitive AI](https://www.youtube.com/watch?v=aR5N2Jl8k14&t=332s).
* I also think about that [AI won't always get what we want](https://www.youtube.com/watch?v=OhCzX0iLnOc) and might be dangerous. We had to set right boundaries to avoid problems.
* I loves AI but I don't trust it.

# 2020-03-25 #

* I watched a lot of videos about ["Sophia the robot"](https://en.wikipedia.org/wiki/Sophia_(robot)). One of my favorite one is when she was on the ["Tonight showbotics" with Jimmy Fallon](https://www.youtube.com/watch?v=G-zyTlZQYpE)
* I watcehed [AlphaGo - The Movie | Full Documentary](https://www.youtube.com/watch?v=WXuK6gekU1Y&feature=youtu.be) and I think Lee Sedol is really amazing. He even won once.
* I read through [Elements of AI - Introduction to AI - Chapter 1: What is AI?](https://course.elementsofai.com/1). I think it truly hard to define AI
* I practiced to made [todoapp](https://todoapp--hanklu2020.repl.co/) with python. Ps. You have to wait until the environment has been set up.
* I also started to use [Jupyter Notebook](https://jupyternotebook--hanklu2020.repl.co/tree?).
* I think deep learning is a interesting topic to learn. If machine can learn by this way, human maybe can do the same.

# 2020-04-01 #

* I watched Deep learning, [chapter 1](https://www.youtube.com/watch?v=aircAruvnKk&feature=youtu.be) and [2](https://www.youtube.com/watch?v=IHZwWFHWa-w&feature=youtu.be). I think it's amazing that we use layers to identify things step by step.
* I tried to learn [tensorflow](https://www.tensorflow.org/) with [jupyter notebook](https://jupyter.org/). I spent hours to solve problems but It kept crashing.

# 2020-04-08 #

* I started to use Colab Notebook to learn tensorflow. I watched this two video, [Deep Learning with Python, TensorFlow, and Keras tutorial](https://www.youtube.com/watch?v=wQ8BIBpya2k&list=PLgNYBtKiwLjgMvlgZRvhlZ86Ii6ENWH6F&index=4&t=1040s) and its [Part 2 Loading in your own data](https://www.youtube.com/watch?v=j-3vuBynnOE&list=PLQVvvaa0QuDfhTox0AjmQ6tvTgMBZBEXN&index=2) and followed them steps by steps.
* I shared the projects I made with a friend. He was amazed.
* I borrowed the book [Automate the Boring Stuff with Python: Practical Programming for Total Beginners](https://weblis.lib.ncku.edu.tw/search~S1*cht?/XAutomate+the+Boring+Stuff+with+Python%3A+Practical+Programming+for+Total+Beginners&searchscope=1&SORT=D/XAutomate+the+Boring+Stuff+with+Python%3A+Practical+Programming+for+Total+Beginners&searchscope=1&SORT=D&SUBKEY=Automate+the+Boring+Stuff+with+Python%3A+Practical+Programming+for+Total+Beginners/1%2C2%2C2%2CB/frameset&FF=XAutomate+the+Boring+Stuff+with+Python%3A+Practical+Programming+for+Total+Beginners&searchscope=1&SORT=D&2%2C2%2C) and read a little bit. I think it's quite boring.

# 2020-04-15 #

* I didn't check the moodle during the day time so I didn't realize there is a synchronous online lecture today until later.
* I was late for my next class beccause I was too focus on my python practices.
* I practice the first two chapters from the book [Automate the Boring Stuff with Python: Practical Programming for Total Beginners](https://weblis.lib.ncku.edu.tw/search~S1*cht?/XAutomate+the+Boring+Stuff+with+Python%3A+Practical+Programming+for+Total+Beginners&searchscope=1&SORT=D/XAutomate+the+Boring+Stuff+with+Python%3A+Practical+Programming+for+Total+Beginners&searchscope=1&SORT=D&SUBKEY=Automate+the+Boring+Stuff+with+Python%3A+Practical+Programming+for+Total+Beginners/1%2C2%2C2%2CB/frameset&FF=XAutomate+the+Boring+Stuff+with+Python%3A+Practical+Programming+for+Total+Beginners&searchscope=1&SORT=D&2%2C2%2C) and read a little bit. I think it's quite boring.
* I think my python is getting better.
* I met a speaker who use python and javascript to build a website. We had a great conversation and he gave me some advices.

# 2020-04-22 #

* Teacher made a videocall through moodle. Some people have some rude questions but teacher still answer them patiently.
* I found out one of the links wasn't work and I let the teacher know about that.
* I watched [TensorFlow and Deep Learning without a PhD, Part 1](https://www.youtube.com/watch?v=u4alGiomYP4&feature=youtu.be) a little bit but I still need to finish it.

# 2020-04-29 #

* I don't understand what is cross entropy when I watched [TensorFlow and Deep Learning without a PhD, Part 1](https://www.youtube.com/watch?v=u4alGiomYP4&feature=youtu.be). [towardsdatascience.com](https://towardsdatascience.com/entropy-cross-entropy-kl-divergence-binary-cross-entropy-cb8f72e72e65) gave me a good answer.
* The code they provided are too old to use. I kept getting errors from [Martin Gardner's Colab](https://colab.research.google.com/github/GoogleCloudPlatform/tensorflow-without-a-phd/blob/master/tensorflow-mnist-tutorial/keras_01_mnist.ipynb#scrollTo=KIc0oqiD40HC).
* I like the ideas that it needs to include the shape and it should kill some  neurons.
* Overfitting is a new concept to me. It needs to be avoided.
* I learn to control motors by importing python tkiner and GPIO to build classes.

# 2020-05-06 #

* I sent a email to the group members. None of them responded me except one that he said he's not in the class any more.
* I read through point 10 and 11 including their videos. I think stop button is a interesting topic to think about.
* Engaging is a better way of learning. Score can motivate robot but its kind of dangerous because they might do something wrong in order to get more score.
* Let the score of stop button equal to others is a good idea but still dangerous.
* I think about the group project, it'll be great if we can seperate good and bed hand writing so it won't damage the good hand writing recognition.

# 2020-05-13 #

* I watched a few AI game projects, such as [MarI/O](https://www.youtube.com/watch?v=qv6UVOQ0F44) and [the World's Hardest Game](https://www.youtube.com/watch?v=Yo2SepcNyw4&t=1s). Machines can play so many hard games that I can't accomplish. They often do things not in a beautiful way but they still can win.
* I also watched [9 Most Advanced AI Robots](https://www.youtube.com/watch?v=Jky9I1ihAkg). I was wondering why there's no sophia the robot.
* I know May 31 is coming soon so I do the AI success story ppt a bit more. I watched [a lecture from MIT](https://www.youtube.com/watch?v=Z5Pw5eWItiw&t=253s). Google had a joke in 2013 about [Google note](https://www.youtube.com/watch?v=VFbYadm_mrw) and they are actually start to [working on it](https://ai.googleblog.com/2019/10/learning-to-smell-using-deep-learning.html).
* I like the olfaction topic.

# 2020-05-20 #

* We had a physical class today and I like the way that teacher gave us question to ponder.
* I met my team member, named Mauricio, who is a very energetic guy. He had a lot of good comments in the class.
* I am still watching this video[In the Age of AI](https://www.youtube.com/watch?v=5dZ_lvDgevk). I like what what a person said, "We are overcoming the limitation of our mind."

# 2020-05-27 #
* We set up a time to meet to discuss about the project but my partner doesn't know how to use python and he didn't bring his computer. So I spend time to teach him some code on colab.

# 2020-06-3 #
* I love the class that teacher provides. The electrical car will be cheaper which gives me different thinking about the future.
* We found a time to meet on Sunday again. The accuracy of the digit recognition is higher than 99%. We are going to film next time.

# 2020-06-10 #
* I watched the videos, [The Digital Skills Gap and the Future of Jobs 2020 - The Fundamental Growth Mindset](https://www.youtube.com/watch?time_continue=195&v=Y9FOyoS3Fag&feature=emb_logo) and [Hilbert's Curve: Is infinite math useful?](https://www.youtube.com/watch?v=3s7h2MHQtxc)