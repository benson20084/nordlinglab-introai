This diary file is written by YuShan Lin (N16090120) in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-19 #

* Read an article about preditive maintenance of machin tools system.
* I think it's really useful for manufacturer factories.
* Learn what data can use to train AI to predict the condition of machines and the tools.

# 2020-03-26 #

* It's hard to give a general definition to AI.
* Sometimes we can give a definition by properties of AI, like autonomous, adaptively, etc.
* Some related fields of AI are Deep Learning, Data Science, Machine Learning, etc.

# 2020-04-02 #

* During the spring break, I read the toturials about python the Professor gave.
* Seems it need to consume a lot of time for me to learn it.
* That's my first time to know about Tensorflow, still try to get familiar with it.

# 2020-04-09 #

* Successful to install Python and Tensorflow.
* Know how to work with this two tools.
* Hope I can learn them smoothly in the future.

# 2020-04-16 #

* Today, Professor gave us a online lecture and also answer some questions for us.
* Need to catch up the topic for the now week.
* Keep learning about Python and Tensorflow.

# 2020-04-23 #

* Professor explained the convolution layer to us, and tell us about the group project.
* I watched the video about the Neural Network that Professor provided and know more about that.
* Neural Network can be think as an complex function.
* Now most of people are using ReLU function on Neural Network instead of sigmoid function.

# 2020-04-30 #

* The steps of convolution neural networks are convolution, normalized, and finally pooling.
* Convolution layers will rectified linear units and pooling layers.
* All of these steps can repeat and get more deep stacking.
* The voting weight depends on how strongly a value predicts what result should be.

# 2020-05-07 #

* Today Professor has announced which group we belong to.
* The topic of group project is about using the best possible neural network for recognition of digits.
* We have to think deeply about how the technology we work on today looks in 5 years, in 10 years, etc.

# 2020-05-14 #

* Professor has tranferred the online course to the physical course, annd we will need to attend the class once every two weeks.
* Today we watched the video about ethics and the danger of algorithm bias.
* We discussed the topic about 'what AI should and shouldn't  do', 'have we met the algorithm bis problem?' and 'how to detect the fake video', etc.
* By discussing the topic above, I got a lot of new perspective from other classmates.

# 2020-05-21 #

* The thought about automation
	* People always said that the machines (or robots) will take our jobs, but I don't think so. I think the machines can simplfied and help us on our work rather than take whole our job.
	* People can be engaged in less labor. But we still have some work that can't use AI or robots to instead of, like some work that need to social with people or some work that need high flexibility that only human can achieve.
	* Also because we don't need to spend time on manual, low-skilled work, we can have time to learn the new things or improve ourselves skills.
	* So rather than being worried about the machines will take our job, we should think about what we can do to adapt the new work mode.
	
# Article summary #

* Distributed representations of words have shown to provide useful features for various tasks in natural language processing and computer vision. While there seems to be a consensus concerning the usefulness of word embeddings and how to learn them, this is not yet clear with regard to representations that carry the meaning of a full sentence.
* Two questions need to be solved in order to build such an encoder, namely: what is the preferable neural network architecture; and how and on what task should such a network be trained.
* Models learned on NLI can perform better than models trained in unsupervised conditions or on other supervised tasks. 
* Authors have compared 7 different architectures to see which architecture can give the best performance.
* By exploring various architectures,authors showed that a BiLSTM network with max pooling makes the best current universal sentence encoding methods, outperforming existing approaches like SkipThought vectors.

# 2020-05-28 #

* Today, we talking about our dream job 10 years from now.
* Discussed the difference between 2nd industrial revolution and 3rd one.
* Professor showed us how industrial revolution and innovation enconomic impacts in different aspects, like in automobile, medical, and so on.
* We will have another discussion next weak.

# 2020-06-04 #

* Today, Professor is talking about how automation impact the economic and industrial.
* We also discussed about how automation will impact our dream job and work security we prefer.
* Next week we have a presentation about the article we read.

# 2020-06-11 #

* We gave a pressntation to the article summary today.
* Next time we need to present the result of the group project and take the final exam.
* There are several topic related to the supervised learning, and I think I have known basic concept more about how to apply the supervised learning to different application.