
This diary file is written by Ting You Liu F94086084 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-21 #

* find success stories of AI(healthcare industries)
* read success stories 

# 2020-03-25 #

* I read "Elements of AI_Introduction to AI_Chapter 1:What is AI?"
* For the defining part,I get to know some examples of AI and also the operation mode behind it.
* The reasons of why AI is so nebulous is pretty interesting.For example,tasks that we thought easy or hard might seem differently to AI.
* In the second section,it mainly discuss about the clear defination of the words related to the field of AI.
* Eugene Goostman passed the Turing test,which means AI can make machine or robot functions more humanize.

# 2020-04-01 #

* Watch the video of  Python Programming (Automate the Boring Stuff with Python)lesson one to lesson four on youtube.
* In the lesson I get to learn the basic syntax of python.
* The teacher speaks in a normal spead,which makes me easy to follow on his lecture.
* Also i feel that python is a language that is really straight foward.
* Overall,I learn alot this week.
# 2020-04-08 #
* Watch the 3blue1brown visualisation on neural networks - "But what is a Neural Network? | Chapter 1"
* This video mainly discuss about now neural network function.
* It is about the connection of different function.
* Learning-finding the right weights and biases.
* Those function mentioned in the video is a bit complicated.
# 2020-04-15 #
* Today's class is mainly about the background of the professor.
* Lots of people asked questions about synthetic biology,which is pretty interesting.
* Also we did some introduction of ourseif.
* I am looking forward to take the next class.
# 2020-04-27 #
* Read the overview of python.
* Find some video about python on the internet.
* Read the tutorial of python.
# 2020-05-01 #
* Watch python codex's video on youtube.
* It's divided into thirty days courses,which is clear and understandable.
* I also read about Tensorflow.I think it ia a bit confusing since I am a beginner.
* I learn things about inheritance and it's pretty cool.
* Overall,I learn alot this week.Only the part of tensorflow is confusing.
# 2020-05-09 #
* Watch the video "Gradient descent, how neural networks learn | Chapter 2" by 3blue1brown based on the MNIST example in Neural Networks and Deep Learning by Michael Nielsen.
* Watch the video "What is backpropagation really doing? | Chapter 3" by 3blue1brown.
* Write some exercise code.
* This week professor answered some questions from other classmate,which are some questions I didn't think of.
# 2020-05-17 #
* Watch"tensorflow and deep learning without a phd"on youtube.
* It's a bit hard to totally understand.
* However some parts were pretty interesting.
* Discuss about group project.
# 2020-05-25 #
* Take physical class.
* Discuss about the ethic issues behind AI.
* I like physical class more than online course.
* There we be more interaction between classmates.
# 2020-05-30 #
* Work on group project.
* Use the data in MNIST to train the model.
* Write the code.
* Figure out the data training process.
# 2020-06-10 #
* group  project
* record video
# 2020-06-14 #
* group project
* film the video