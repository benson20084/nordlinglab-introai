This diary file is written by WEI-HSUN KAO C440640556 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.


# 2020-03-21 #

* It is my first time to write down the diary on the internet, especially using bitbucket for the course.
* It is serendipity that I found this course in moodle on some special night when I finished my work before sleep.
* As a student of department of physics,if I don't learn more about information technology and programming ability,I am afraid I will be eliminated in the near future.
* I hope I can learn a lot from this course and I am looking forward to the course in this semester.

# 2020-03-28 #

* I read some teaching material about AI ,especially the concept of tensorflow
* In this week, I tried to install tensorflow in my computer but there are something wrong and it seemed not to be solved easily.
* What's worse ,the screen of my notebook was borken this morning,it also took me lots of time to fix it.
* In the afternoon, I spent some time studying python and using it to my work of the solid state physics.
* I hope in the near future deep learning or machine learning can be using in my work. 

# 2020-04-4 #
* There are so many things have to be done,they cost me lots of time.
* After I read the article "Elements of AI" did I know there is a methodology about "Philosophy of AI".

# 2020-04-11 #
* This week, I tried to study linear regression and how it work using tensorflow.
* My notebook's screen shut down from time to time, it bothered me a lot.

# 2020-04-19 #
* This Wedensday was first time professor taught a class , I was astonished because I didn't notice the moodle
  until I got the gmail in the afternoon.
* The screen is still shut down ,this will be returned to the original produced factory for the checking, 
  which will spend around 3 to 5 days.
* I also studied some linear regression and how it worked.



