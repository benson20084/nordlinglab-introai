This diary file is written by Vukile Masango E64047034 in the course Introduction to Artificial Intelligence and 
Deep Learning for Engineers and Scientists.

# 2019-02-20
* professor gave a brief introducton on what the course is about and what to expect from learning this course.
* I was happy about the lecture because it built my interest in the subject of artificial intelligence 

# 2019-02-27
* Today I learnt that AI has a huge scope and is inclusive of a whole lot of different fields that focus on data collection or processing.
* I left the lecture with the conclusion that the core existence of artificial intelligence is to collect and process data using computer systems ,for knowledge
* The professor also introduced the diary.
* The diary seems to be a great  way to reflect what I learnt on each day of the class, also its already a way to get us involved into the technology of computer 
  sytems used to store data nd information.

#2019-03-06
* Today the lecture was about python
* I have mixed feelings about the lecture today , I was glad the professor took into accunt that there may be some students who never wrote codes before however 
  I felt he explained everything very quick and Im afraid some beginners could be confused.
  
# 2019-03-13
* Today's lecture was basically about practicing how to write codes in python.
* It was at introductory level so we read a few descriptions and did some exercises to test our understanding 
* I was a bit worried that the language would be a bit challenging however it felt pretty straightforward.
* This course actually deserves to be worth two credits 

# 2019-03-20
* I learn better with visual so for me watching the video was awesome. The whole time I was there tinkng of the large amounts of data involved in this and
  and the complexity involved.
* I also wondered if it ever happens that the artificial network finds some questions ,that may be concidered easy for humans, to be difficult?

# 2019-03-27
* Today's class was straight forward we discussed more about the neuro network.
* also had an interesting discussion about why all models are wrong. 
* t was exciting to see more visuals 

# 2019-04-10
* today's lecture was about gradient descent 
* There were alot of new terms which I did not understand and for that reason Im not sure if I learnt anything in todays lecture.
* Next week we start practice again , hopefully then I will understatnd what professor was explainng during the lectures 

# 2019-04-17
* today we learnt how to use python commands on TensorFlow in order to create and train a model
* I did not understand any of the commands and I felt it was difficult, however I understood the prupose of the model 
* I'm starting to feel nervous about this course as it gets more and more confusing week by week. from what I see, one hour per week is not enough tO introduce and explain all the concepts.
* there is a lot of information to grasp and so little time

# 2019-04-24
* today we were practising how to use tensor flow to create and train a model.
* I could only go as far as running the codes but could'nt start the exercises.
* as far as I  think of the experience , I found that the commands are hard to understand.  
  I had absolutley no idea what each command line meant and hence even when I attempted to start the exercise ,I was clueless of what to alter.

# 2019-05-01
* We did more TensorFlow practise today 
* after the last practice I was compeled to go and study more about tensr flow from the internet so when the teachr went through the program again it became more clear
* troubleshooting took most of my time but I eventually was able to do the exercise. 
* I feel a little different about the difficuty of this course , it eventually comes back to how willing you are to learn and get your hands dirty>
* I learnt better from the peer to peer discussions .

# 2019-05-08
* today we reflected through the course objectives. touching on every part that we initially targeted to understand by the end of the semester.
* by the time we went through the whole list , I still felt like I dont understand most of these networks in artificial interlligence .
* the smart phone example actually made me realise that deep learning is indeed difficult and that the "drag and drop" or rather , using the final product is
  much more easy, however, it also helped me realise that if it was easy then it probably would not be called DEEP LEARNING.
* Proffessor also answered some questions from fellow classmates 

# 2019-05-15
* In today's lecture I was more intrigued by the video we watched. It got me thinking about the future and I wondered how close to we are to having the black mirror effect 
  hereby artificial intelligence takes over basically the whole world.
* The quetion that was asked by one classmate about what happens when people just want to do those basic jobs that are rapidly being wiped by AI.? 
  these I believe is one f the negavtive externalities that come with this industry . we indeed cannot all become technicians.
  
# 2019-05-22
* There is someting about TED talks that is always so captivating. Today we watched tedtalks about AI application topics and they covered a lot 
  of the stuff we have learnt in class.
* It was refreshing to see them being applied in realistic and practical situations. 
* As we  discussed I was able to see others perspective on the topic and learn from them.

# 2019-05-29
* Today we discussed about the final project with our team mates . my group was missing one person so we have not picked a leader yet.
* even with just the two of us we were still able to discuss and decide what we would want to do moving forward 
* I beleve we made good progress so far

# 2019-06-05
* we continued to discuss about the project. mainly working on improving our % error.

# 2019-06-12
* today we presented our project outilining each and every stepa and shared our experience while building the code..
* honestly it was  lovely experience and I noticed thet indeed peer to peer educatin is powereful. i don not think I 
  will easily forget what I learnt whilst working with my team mates as compared to what I learnt in class 
* We somehhow were able to create 4 codes and evaluate whihch one has the best accuracy . all four codes were differnt.

