This diary file is written by Guan Lun Huang F74079041 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-21 #

* It took some time for me to totally figure out what I should do in each lecture and it's probably because there are a lot of hyperlinks in the bullet points in each week lecture and if I miss any of it, I might not be able to finish the weekly assignment perfectly.
* In order to find out which successful story of AI I want to turn in in this class, I inspect all of the news I recently hear and I indeed have some cases avaible. But eventually my final topic is found on Youtube, which is the newest topic I've ever heard.
* Reading the PPT of AI stories by other classmates not only broadened my vision on the applicatoin of AI but also helped me learn some representation skill on PPT.
* The website for AI learning(Elements of AI) at first focuses on the definition and philosophy of AI, providing the concise view on this topic.
* I also found there are some myths in my thoughts against AI.
* What I took for granted about AI before is now changed, in a good way.
* I need to improve my skill on markdown language because there are actually some good examples by other classmates.

# 2019-03-29 #
* Since I expect to learn Python this week, I try to learn from the official documentation.
* [Python 3.6.10 documentation](https://docs.python.org/3.6/) is the version I evaulate as the most stable version for our AI API.
* Topic : Getting help and comments, Printing, Conditional statements and loops
	* Getting help and comments
		* Since I already knnow how to use an IDE, such as Pycharm, I try to focus on how Python work as an interpreter language and on the usage of Python as a whole.
		* Interesting fact : The language is named after the BBC show “Monty Python’s Flying Circus” and has nothing to do with reptiles.
		* By default, Python source files are treated as encoded in UTF-8.
		* Note that a secondary prompt on a line by itself in an example means you must type a blank line; this is used to end a multi-line command.
		> \# this is the first comment  
		spam = 1  \# and this is the second comment  
		\# ... and now a third!  
		text = "\# This is not a comment because it's inside quotes."  
	* Printing  
		* Fibnacci Funciton
		> \>>> a, b = 0, 1  
		\>>> while b < 1000:  
		...     print(b, end=',')  
		...     a, b = b, a+b  
		...  
		1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,  
	* Conditional statements and loops
		* if Statement
		> \>>> x = int(input("Please enter an integer: "))  
		Please enter an integer: 42  
		\>>> if x < 0:  
		...     x = 0  
		...     print('Negative changed to zero')  
		... elif x == 0:  
		...     print('Zero')  
		... elif x == 1:  
		...     print('Single')  
		... else:  
		...     print('More')  
		...  
		More  
		* for Statement
		> \>>> for w in words[:]:  # Loop over a slice copy of the entire list.  
...     if len(w) > 6:  
...         words.insert(0, w)  
...
\>>> words  
['defenestrate', 'cat', 'window', 'defenestrate']  
		* range() Function
			1. how to use
		> \>>> for i in range(5):  
...     print(i)  
...  
0  
1  
2  
3  
4  
			2. with a step
			> range(5, 10)  
			>   5, 6, 7, 8, 9  
			> range(0, 10, 3)  
			>    0, 3, 6, 9  
			> range(-10, -100, -30)  
			>   -10, -40, -70  
			
* I learnd a lot of MarkDown syntax when I wrote this diary.
# 2019-04-05 #
* Topic of this week : Variables and functions, Reading and writing files, Error handling
	* Variables and functions
		* There are tools which use docstrings to automatically produce online or printed documentation, or to let the user interactively browse through code; it’s good practice to include docstrings in code that you write, so make a habit of it.
		> \>>> def fib(n):    # write Fibonacci series up to n  
...     """Print a Fibonacci series up to n."""
		* The actual parameters, or arguments, are passed using call by value (where the value is always an object reference, not the value of the object).
		* In fact, even functions without a return statement do return a value, albeit a rather boring one. This value is called None (it’s a built-in name). Writing the value None is normally suppressed by the interpreter if it would be the only value written.
		> \>>> fib(0)  
\>>> print(fib(0))  
None
		* Python function also got the overloading-like feature as in C++ but called "multimethods", which is based on dynamic binding technique.
		* In a function call, keyword arguments must follow positional arguments.  
		> def parrot(voltage, state='a stiff', action='voom', type='Norwegian Blue'):
parrot(1000)                                          \# 1 positional argument
parrot(voltage=1000)                                  \# 1 keyword argument  
parrot(voltage=1000000, action='VOOOOOM')             \# 2 keyword arguments  
parrot(action='VOOOOOM', voltage=1000000)             \# 2 keyword arguments  
parrot('a million', 'bereft of life', 'jump')         \# 3 positional arguments  
parrot('a thousand', state='pushing up the daisies')  \# 1 positional, 1 keyword  
		* Unpacking Argument Lists  
			1. write the function call with the *-operator to unpack the arguments out of a list or tuple  
		> \>>> list(range(3, 6))            # normal call with separate arguments  
[3, 4, 5]  
\>>> args = [3, 6]  
\>>> list(range(*args))            # call with arguments unpacked from a list  
[3, 4, 5]  
			2. dictionaries can deliver keyword arguments with the **-operator  
		> \>>> def parrot(voltage, state='a stiff', action='voom'):  
...     print("-- This parrot wouldn't", action, end=' ')  
...     print("if you put", voltage, "volts through it.", end=' ')  
...     print("E's", state, "!")  
...  
\>>> d = {"voltage": "four million", "state": "bleedin' demised", "action": "VOOM"}  
\>>> parrot(**d)  
-- This parrot wouldn't VOOM if you put four million volts through it. E's bleedin' demised !
	* Reading and writing files
		* example :  
		> \>>> f = open('workfile', 'w')
			* r : read ( default )
			* w : write
			* a : append
			* r+ : read & write
		* dump & dumps for JSON format
	* Error handling
		* example :  
		> \>>> while True:  
...     try:  
...         x = int(input("Please enter a number: "))  
...         break  
...     except ValueError:  
...         print("Oops!  That was no valid number.  Try again...")  
...
* I learnd a lot of new concepts for programming such as "Lambda Expression", "JSON serializing and deserializing".

# 2019-04-12 #
1. Watch
	- The visualisation of an artificial neural network - "[But what is a Neural Network? | Chapter 1" by 3blue1brown](https://www.youtube.com/watch?v=aircAruvnKk&feature=youtu.be).
		- **activations** : neurons holding numbers
		- **weight** : the connection lines contains betwenn the layers
		- **hidden layers** : middle layers that intend to abtract important characteristics 
		- recognize numbers using The **MNIST** database
		- activation function using **Sigmoid function** a.k.a. **logistic curve**
		- ![](https://i.imgur.com/Indot5e.png)
		- The weights and biases make the whole variables for a neuron network.![](https://i.imgur.com/L8ZfZKR.png)
		- **Learning** means finding out fit-in variables
		- ![](https://i.imgur.com/Xko78Qo.png)
		- Simplified version of the matrix calculation.![](https://i.imgur.com/nocj5Ik.png)
		- Sigmoid is rather "Old School" ways. Instead, ReLU is more recommended for activation function. ![](https://i.imgur.com/WUjze7f.png)

2. Read Ch. 5.1-5.3 in [Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning](http://www.deeplearningbook.org/)
	- What do we mean by learning? Mitchell (1997) provides a succinct deﬁnition:
		- “A computer program is said to learn from **experience E** with respect to someclass of **tasks T** and **performance measure P**, if its performance at tasks in T, asmeasured by P, improves with experience E.”
	-  Famous data set : **Iris** dataset
	-  Supervised ML (Looking for the function from x to y, associated with Labels or Targets) vs Unsupervised (looking for distribution as a whole, e.g density estimation)
	-  **Reinforcement learning algorithms** interact with an environment, sothere is a feedback loop between the learning system and its experiences.
	-  A **design matrix** is a matrix containing a diﬀerent example in each row. Each column of the matrix corresponds to a diﬀerent feature.
	-  Linear Rregression
		-  ![](https://i.imgur.com/jFOkQvr.png)
		-  Minimizing the **MSE** (mean squared error) is a way to find the prediction function.
	-  **Underﬁtting** occurs when the model is not able to obtain a suﬃciently low error value on the training set.
	-  **Overﬁtting** occurs whenthe gap between the training error and test error is too large.
	-  We can control whether a model is more likely to overﬁt or underﬁt by altering its **capacity**.
	-  One way to control the capacity of a learning algorithm is by choosing its **hypothesis space**. Generalizing linear regression to include polynomials increases the model’s capacity.
	-  **Occam’s razor**(c. 1287–1347) states that among competing hypotheses that explain known observations equally well, we should choose the “simplest” one.
	-  ![](https://i.imgur.com/hJrba7b.png)
	-  The **no free lunch theorem** for machine learning (Wolpert, 1996) states that, averaged overall possible data-generating distributions, every classiﬁcation algorithm has the same error rate when classifying previously unobserved points.
	-  There are many other ways of expressing preferences for diﬀerent solutions, both implicitly and explicitly. Together, these diﬀerent approaches are known as **regularization**.
	-  Regularization is any modiﬁcation we make to alearning algorithm that is intended to reduce its **generalization error** but not its training error.
	-  ![](https://i.imgur.com/raibFPN.png)
	-  The values of **hyperparameters** are notadapted by the learning algorithm itself (though we can design a nested learning procedure in which one learning algorithm learns the best hyperparameters for another learning algorithm).
	-  To solve overfitting of hyperparameters, we need a validation setof examples that the training algorithm does not observe. We always construct the validation set **from the training data**.
	-  Typically, one uses about **80 percent** of the training data for **training** and **20 percent** for **validation**.
3. Watch the following parts of the tutorial ["TensorFlow and Deep Learning without a PhD"](https://www.youtube.com/watch?v=u4alGiomYP4&feature=youtu.be) by Martin Görner.
	- I downloaded the code and install CUDA 10.1 for tensorflow 2.1 But it ended up failed due to the code fitting in older version of tensorflow.
	- Finally I got the environment set up with tensorflow 1.14.
	- I found the data visualization of the presentation is beyond amazing. I would consider using it in the future.
	- The presentation can be summed up with a picture. ![](https://i.imgur.com/4gOXfmk.jpg)
	- The revolution of the NN (The name of .py file tells pretty much every thing)
		- mnist_1.0_softmax (**92%**)
		- mnist_2.0_five_layers_sigmoid (**97% slower, noise for testing error and training error**)
		- mnist_2.1_five_layers_relu_lrdecay (**98% much faster than sigmoid, less noisy by learning rate decay, overfitting**)
		- mnist_2.2_five_layers_relu_lrdecay_dropout (**drop out lessen the hyperparameters, but still not useful here, losing shape information since the previous flatten image**)
		- mnist_3.0_convolutional (**almost 99%, still overfitting**)
		- mnist_3.1_convolutional_bigger_dropout (**99%!!! dropping out lower the degrees of freedom**)
		- mnist_4.0_batchnorm_five_layers_sigmoid
		- mnist_4.1_batchnorm_five_layers_relu
		- mnist_4.2_batchnorm_convolutional (**99%, much more faster with batch normalization**)
		
# 2019-04-19
1. Watch
	- The explanation of filtering, convolution, pooling, normalisation, ReLU, and fully connected layers in ["How Convolutional Neural Networks work" by Brandon Rohrer](https://www.youtube.com/watch?v=FmpDIaiMIeA&feature=youtu.be&t=120) from 2:00 to 16:44. Brandon Rohrer uses a simple example to illustrate these concepts.
		- **filter** used for **feature extraction**
		- **convolution layers**: ![](https://i.imgur.com/tN0Yu7i.png)
		- **pooling** ![](https://i.imgur.com/EUVjss2.png)
		- **max pooling** ![](https://i.imgur.com/stC1JL0.png)
		- **Normalization - ReLu** ![](https://i.imgur.com/v7kfxD8.png)
		- **Deep Stacking** ![](https://i.imgur.com/g61ssmo.png)
		- **Hyperparameters** ![](https://i.imgur.com/hfEYhI7.png)
		- ![](https://i.imgur.com/stQ6no2.png)
2. Read Ch. 9.1-9.3 in [Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning](http://www.deeplearningbook.org/)
	- Convolution consists of an **input** and a **kernel**. The ouput is also called **feature map**.
	- In machine learning applications, the input is usually a **multidimensional array** of **data**, and the kernel is usually a **multidimensional array** of **parameters** that are adapted by the learning algorithm. We will refer to these multidimensional arrays as **tensors**.
	- Many neural network libraries implement a related function called the **cross-correlation**, which is the same as convolutionbut without ﬂipping the kernel
	- An example of 2-D convolution. ![](https://i.imgur.com/TR1honT.png)
	- Convolution leverages three important ideas that can help improve a machine learning system:**sparse interactions**, **parameter sharing** and **equivariant representations**.
		1. **sparse interactions**
			- If there are m inputs and n outputs, then matrix multiplication requires m × n parameters, and the algorithms used in practice haveO(m × n) run time. Our goal : make m to a smaller number k. ![](https://i.imgur.com/c7ObsQA.png)
		2. **parameter sharing**
			- Parameter sharing refers to using the same parameter for more than one function in a model.
			- One can say that a network has **tied weights**, because the value of the weight applied to one input is tied to the value of a weight applied elsewhere.
			- Convolution is dramatically more eﬃcient than dense matrix multiplication in terms of the memory requirementsand statistical eﬃciency.
		3. **equivariant representations**
			- Equivalent Opeations: a functionf(x) is equivariant to a functiongiff(g(x)) =g(f(x)).
	- A typical layer of a convolutional network consists of 3 stages
		1. The layer performs several **convolutions** in parallel to produce a set of **linear activations**.
		2. Each linear activation is run through a nonlinear activation function, such as the rectiﬁed linear activation function. This stage is sometimes called the **detector stage**.
		3. We use **apooling** function to modify the output of the layer further.
	- 2 points of view about the convolution layers ![](https://i.imgur.com/mXGsc1M.png)

	- The max pooling(Zhouand Chellappa, 1988) operation reports the maximum output within a rectangular neighborhood. ![](https://i.imgur.com/grOLMMN.png)

	-  Pooling over **spatial** regions produces **invariance to translation**.