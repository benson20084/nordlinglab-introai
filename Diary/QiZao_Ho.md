This diary file is written by QiZao Ho E34085337 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-03-04 #

* This is the first lesson.
* We do not needed to go to classroom as this course become a online course.
# 2019-03-12 #

* This is the second lesson.
* I am waiting for the instruction of professor.
# 2020-03-22 #

* This is my first time uses this system and writes diary.
* I had read some classmate's diaries. 
* After read successful story of others done,I search the information about AI.
* I have done the successful story of AI which about caught masquitoes and analyse the pathogon.
* Read the pdf about success stories which name 'an Artificial Intelligence outperform humans' uploaded by professor.
* I am amazed that AI already outperform human in many fields.

# 2020-03-27 #
* I had read 'What is AI'.
* I have more realize the defination of AI.
* I am interest on the philosophy of AI.

# 2020-04-05 #
* I had watched the 3blue1brown visualisation on neural networks - "But what is a Neural Network? | Chapter 1"

# 2020-04-12 #
* I had read the material uploaded by professor.

# 2020-04-19 #
* First time met professor online.
* More understanding about this class.
* More understanding about professor.

# 2020-04-26 #
* Read Ch. 5.9 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning.
* Read "Using neural nets to recognize handwritten digits" in Neural Networks and Deep Learning by Michael Nielsen.
* Try learning python.

# 2020-05-02 #
* Read the explanation of filtering, convolution, pooling, normalisation, ReLU, and fully connected layers in "How Convolutional Neural Networks work" by Brandon Rohrer from 2:00 to 16:44. Brandon Rohrer uses a simple example to illustrate these concepts.
* Read Ch. 9.1-9.3 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning.
* Continue study python.

# 2020-05-10 #
* Read Ch. 5.9 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
* Read "Using neural nets to recognize handwritten digits" in Neural Networks and Deep Learning by Michael Nielsen.
* Check the group.

# 2020-05-17 #
* Found group project teammate.
* Read the slides about group project.