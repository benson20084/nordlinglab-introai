This diary file is written by Ming-Hsien Hsu F84054027 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-20 #

* I watched the welcome video.
* I read some success stories of AI from the course google drive.
* I made a slide about using machine learning to design optical devices.
* I read the article"Wearable Sensors for Prodromal Motor Assessment of Parkinson's Disease using Supervised Learning", but couldn't fully understand the content.

# 2020-03-29 #
* Read "Elements of AI - Introduction to AI - Chapter 1: What is AI?".
* Watched a video on Youtube about regression.
* Turing test is a way to determine whether a computer reached human-level intelligence. In the test, a human interrogator will interact with two player, one is a compouter and the other is a human. If the interrogator can't determine which player is computer, then we say that the computer is as smart as humans.

# 2020-04-05 #
* Learn some basics of python from Nordling Lab - Python and Deep learning tutorial in Colaboratory.
* Read part of python tutorial from the python official website.

# 2020-04-12 #
* Watch python tutorial video on Youtube.

# 2020-04-19 #
* Learn some basics of tensorflow2.0 from NordlingLab_Tutorial_Python_Deep_learning.ipynb.

# 2020-04-24 #
* Watch "But what is a Neural Network? | Chapter 1" by 3blue1brown. I think the way how newral network works is very cool.
* Read Ch. 5.1-5.3 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning.
* Task is not the learning process. We use learning to gain the ability to perform the task. There's different kind of tasks, such as classificatoin, regression, synthsis.  
* Unsupervised learning algorithms learn the properties of datasets without instructor. Supervised learning algorithms experience datasets which each example is associated with a "label".

# 2020-05-03 #
* Watch TensorFlow and Deep Learning without a PhD, Part 1.
* We can stack different layers of newron to achieve better result.
* Convolution layer is suitable for image recognition.

# Article summary #
* Two inertial wearable sensors were used to collect the motion data of upper and lower limbs.
* Performance of two supervised learning algorithm(SVM and RF) were compared on classification.
* The study found that it was better to analyze upper and lower limbs together rather than analyzing them separately.
* Classification with RF were pretty good.