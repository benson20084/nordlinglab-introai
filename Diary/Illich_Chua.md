This diary file is written by Illich Chua I84055018 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-03 # Welcome 

* Welcome video was posted, thanks to all the effort of the Nordling Team (it must have been hard)
* Excited to learn about a whole field that I currently have no idea about

# 2020-03-21 # Success Story HW

* Troubled on what topic I should do my HW on
* It was hard to not repeat a topic as the other students...
* Discovered a lot of application of AI in a variety of unthinkable fields: ocean, pollution, traffic, cooking, etc.
* Most memorable one that I saw and wanted to center my HW on was about cooking (robotic kitchen)
** Decided against it as they were still in the prototype stage
** Another reason for which is because there was a crucial feature in which they currently do not have.
* Moley and Spyce were the two very interesting products 
* Spyce is already operating in the United States, however can only process orders -> retrieve prepared ingredients -> mix  (https://www.youtube.com/watch?v=rfMZfxgbuCw)
* Moley is still a prototype that aims to mimic a real chef's actions in cooking and has access to thousands of recipes on a database
* However, the feature that i was looking forward to was hoping these robotic kitchens can perform early preparation of the ingredients too (washing, cutting, sorting) 
* Another applciation I am currently looking forward to (under development) are AI-assisted detection of drivers using their phones and drunk driving.

# 2020-03-29 # Introduction to AI - Ch 1

* Not "an AI" but "an AI method" -> AI is not a countable noun!
* Personally, in addition to the vague definition of AI, the whole concept seems a little too hard to grasp for me. "How can they make human-like actions and computations into a string of equations?" "How do machines LEARN???"
* Intelligence is not only composed of a single dimension! Howard Garner's 9 intelligences are as follows:
	** Naturalist (nature smart)
	** Musical (sound smart)
	** Logical-mathematical (number/reasoning smart)
	** Existential (life smart)
	** Interpersonal (people smart)
	** Bodily-kinesthetic (body smart)
	** Linguistic (word smart)
	** Intra-personal (self smart)
	** Spatial (picture smart)
	* I think that most people think of AI as the logical-mathematical type, perhaps because thats usually what we develop them to be, since it's much more of an effort for us humans to perform?
	* Another interesting thing about AI anticipate the unknown or learn to based from past experiences? Such was stated in the article, "when someone else opens a door just as you are reaching for the handle, and then you can find yourself seriously out of balance." 
		Although I have seen an application of AI method to be an acrobat that tumbles and jumps, I have to wonder, will the AI know how to balance itself or react accordingly to how a human would in unexpected circumstances?
		Perhaps the way our body's nerves and reflexes are still a much deeper and unknown field for AI learning, but i believe it will be possible.
* Reading the conversation with Eugene Goostman was very interesting. It was making subtle jokes and I can't help but wonder again how is a personality encoded??
* Interested to know MORE about how AI is programmed, but perhaps through a diagram, flowchart, or something more visual to represent the whole thing would be more effective than in words :-)

# 2020-04-30 # Introduction to Python (Getting help and comments, Printing, Conditional statements and loops)

* Wrote an entry a couple of days ago but forgot to commit.... 
* I was hoping there would be perhaps an introductory lecture on the basics of programming, and not immediately jumping to learning how to code.. It will help the students grasp the concept better and spark more interest in learning.
* Went to <https://www.freecodecamp.org/news/a-gentler-introduction-to-programming-1f57383a1b2c/> to learn about the most basic things from programming and it mentioned how there are two shells: CLI and GUI. I think i have only ever used the CLI cmd to check for my ping when i used to play online games.. haha
* Today I learned the basics. It is quite interesting and challenging since I have to look at the examples and type. Perhaps with more practice the codes can come out more naturally just like how I'm typing this without even making much effort! haha
* Created my own Google Colab file to practice :D 
** On the "Printing" example, << print("Here", type(y), "was converted to", type(str(y)))>> Did not work. But as i removed the "TYPE" command, it worked. I searched for the meaning of type but it still doesn't make any sense to me..
* Have to remind myself to put the comments (#) so that I won't forget what I'm doing when I visit my file after a long time!
* Went on Youtube <The Net Ninja> to learn the basics about Python. I learned the code "type," numbers, strings, if statements, lists, etc.
* Learned that there are "expressions" and "actions" and somehow got the ghist of how to differentiate them. An example of action would be .append while for expression .upper. Printing the whole statement for expression is necessary, while only prinitng the list name or modified object in action is necessary
* Successfully did the exercises on my own! It was rough at first but I feel very proud!

# 2020-06-7 # Introduction to Python (Variables and functions, Reading and writing files, Error handling)

* Catching up on past homeworks!
* I learned how to make an interactive mode while learning about functions! It was very interesting and pleasing
* Also it was interesting to see how areas of circles are computed!
* I think I need more practice for the coding to come naturally for me such as typing is!