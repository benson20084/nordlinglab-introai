This diary file is written by Kyle Chien E84056029 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.
---
## 2020-03-18
* Find success stories of AI
* Read article: Lung Nodule Detection
---
## 2020-03-29
* Read article:  Elements of AI - Introduction to AI - Chapter 1: What is AI? 
	* Since I have some background knowledge about AI, I just read through it quickly, but the article do refresh my definition of AI.
---
## 2020-04-12
* Watch the video [But what is a Neural Network? | Chapter 1](https://www.youtube.com/watch?v=aircAruvnKk&feature=youtu.be)
* Extra works : I'm recently working on object detection tasks for a competition, the part of the goal is to detect accurate position of an object and pass the position data to a laser carving machine.
Therefore, I read the paper [CornerNet_Lite: Efficient Keypoint Based Object Detection](https://arxiv.org/pdf/1904.08900.pdf) which can precisely detect the object's corner, 
and I'm now trying to implement it on my project.
---
## 2020-05-10
* I did some Tensorflow practices.
* I successfully trained the cornerNet_Lite mentioned above in my own dataset which is written in pytorch, 
so I'm recently trying to be familiar with Tensorflow and find out what is the difference between them.
---
## 2020-05-17
* Keep doing some Tensorflow practices.
* watch the videos about Gradient descent, Backpropagation, and loss functions.
---
## 2020-05-24
* work on group project.
