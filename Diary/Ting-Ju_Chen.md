This diary file is written by Ting-Ju Chen N18081509 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-22 #

* The article is interesting and inspiring.
* The author explains a lot of the most common questions about AI.
* It's a great start for me to get to know AI.

# 2020-03-29 #

* I tried to spare some time to REVIEW the materials of the past two weeks since i was really busy then, and i don't think i do my best in the previous assignments.
* i am actually floating of the delaying uploading video, because it brings me more time to catch up with the class.
* Programming is important, but truely unstand the idea of AI even matter.
* I'm glad to review the article "What is AI?"
* Perhaps we can keep going on to the topic about neural networks and modeling on week 5.

# 2020-04-05 #

* This week is the spring holiday, but we still have tasks to work on.
* Thankfully, the topic we got this week is really interesting that I even watch the video "But what is a Neuron Network" for THREE tims.
* I am really excited.
* I beleive that the concepts of Neuron Network will help in my recent research.
* It is just AWESOME.

# 2020-04-10 #

* I'm getting used to the learning pattern we have right now.
* I finally find the topic of my success story that I am truely interested in.
* I plan to study the topic of week 6 tomorrow.

# 2020-04-18 #

* I missed the first online class on 4/18.
* There will be another on 4/23 according to moodle.
* I'll make sure I attend the next online class.
* The new upload materials are a lot.
* I took lots of efforts to consume them.

# 2020-04-25 #

* The material of this week (week 8)is not easy and it's a lot.

# 2020-05-01 #

* I found that I mixed up the date of last week's diary.
* There's no material though a title of this week ( week 9 ), "Introduction to Model validation (test/training split, cross-validation, under-/overfitting, bias variance tradeoff)"
* I review the material of week 8.
* Next week we'll have the group project.
* The group project makes me nervous.

# 2020-05-08 #

* We are assigned to different groups this week.
* I'm excited to do this project about applications.
* The topic looks difficult, but I'm sure we'll learn a lot from this. 

# 2020-05-14 #

* Today we have the first off-line class for the first time.
* The off-line class is so much effective than online class to me.
* We talkde about the ethics according to several TED speeches.
* I realy agree with one of the girl's opinion: social change should be priority instead of an afterthought in the AI feild.

# 2020-05-25 #

* I just found out that I forgot to write the diary last week; should've remembered.
* Our team had a meeting last week, and I've learned a lot from my partners.
* We are now working on the project so hard.

# 2020-05-31 #

* I couldn't help but notice that we are reaching June, which means the course is coming to an end.
* When I start to re-check about how I feel to automation, I am surprised that my mind has changed a lot after taking this course.
* I was pessimistic about a future world filled with automation devices. But aftering knowing that people aren't impulsive while dealing with these topics but thinking carefully about ethics things, I'm looking forward to live in a world with more automation.
* As for my future workplan, first of all, I would like to introduce this deep learning algorithm to my own research to enhence the accuracy and efficeincy in the near future. 
* Since this is going to be a general tool, I'm interested in developing in related feilds, though I don't know what exactly it is for now.

# 2020-06-06 #

* Today we have a group meeting.
* We are working on the group project.
* It's interesting. I think I've learned a lot through the discussion. 

# 2020-06-12 #

* I missed the presentation yesterday, so I'll make a YouTube video instead.
* Our group is trying to improve our group project.
* It's really fun to do this by ourself. 
* I think it's time to start reviewing the lesson materials for the final exam.

# Article summary#

* Dropout is a key solution to the problems of overfitting and slow use due to the large network withi deep neural nets.
* How does it work? It's done by dropping units randomly from the network during training. This prevents units from co-adapting too much. Dropout samples from an exponential number of different THINNED the network.
* One of the downbacks of dropout is that increases training time, typically 2-3 times longer than a typical one.
* Supervised learning applicaitons brought up in this articles are: vision&speech recognition, document classification and computational biology.