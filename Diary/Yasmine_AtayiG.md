
This diary file is written by Yasmine Atayi-Guedegbé N16088416 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-15 #

* The development and growth of AI began in the 1950s 
* The rise of AI has allowed the development of image classification so that the model error is lower than the human error  
* AlphaZero is different from AlphaGo because its training starts with a deep neural network and algorithms that knew nothing beyond the basic rules and had to learn how to discover new knowledges
* "Image caption" impressed me. Just by seeing an image, the model can describe the emotions emanating from this image  ! 
* An AI can do better than a dermatologist for the diagnosing of skin cancer... it's worrying about the future of jobs in medicine. 
* The access to AI teaching can be helpful because each child can learn at his or her own pace and the AI may be able to detect gaps where in the classroom this is not obvious

# 2020-03-22 #

* We all know that the advertises on our phone are not random facts. Same about face recognition.
* It is difficult to have an unique definition of AI but what we can retain is that an AI method has to learn by its own in order to improve performances. 
* Machine learning, being a subfield of AI, is the possibility of a system to improve its skills through the collection of data and experience in time 
* “A computer program is said to learn from experience E with respect to some class of tasks T and performance measure P if its performance at tasks in T, as measured by P, improves with experience E.” - Tom M. Mitchell
* It is not clear for me, what deep learning really is. I cannot find a good definition to clearly understand 
* "Robots consist of moving parts and sensors, but to operate some action using these parts, artificial intelligence helps in processing the problem environment, inputs, data", "and based on these data inputs, provide a course of action or output for the robot to operate accordingly"
* The Turing test tries to bring in light the perfomances and skills of a machine vs a human
* The are 2 types of classification in AI : General/Narrow and Strong/Weak AI 

# 2020-04-02 #

* This section of the course will be long because I tried to summarize all about point 5. Introduction to Neural Networks and Modelling

* Neuron networks are inspired by the brain, where a neuron holds a value between 0 and 1 : this number inside the neuron is called its "Activation"
* The plain vanilla is a network consisting of layers of neurons which hold a value of the corresponding pixel
* The last layer has 10 neurons, each representing one of the 10 digits and holding a value which represent how much the system thinks that the given image correspond at the given digit.
* Between the first layer and the last layer, there are hidden layers to construct step by step the digit 
* To move from layer to another, neurons of the previous layer will give a weight to neurons of the next layer, so that each layer associates its activations with that of the next layer to form the digit 
* The weight must be a number between 0 and 1. A common function doing this is called the Signoid function so that the activation becomes a measure of how positive the weight sum is. 
* To the weight sum, we add the "bias" which tells us how high the weight sum should be to activate the neuron

* Let's describe more the concept of machine learning 
* The task : Machine learning tasks are described in terms of how the machine learning system (MLS) should process an example, where an example is a collection of features that we want the MLS to process, represented by a vector X, where each xi is a feature.
* The most common machine learning tasks are : classification, classifications with missing inputs, regression, transcription, Anomaly detection...
* The performance measure : enables us to evaluate the abilties of a machine learning algorithm
* For tasks as classification, classification with missing outputs and transcription, the performance consists in measuring the accuracy : that means the model has to produce the correct output. 
* Usually, we will evaluate the performance using a test set of data, which is different of the data used for the training of the MLS, thus reflecting the real world. 
* The experience : machine learning algorithms (MLA) can be allowed to experience a dataset or not. A dataset is a collection of many examples
* MLA are categorized as unsupervised or supervised. 
* Unsupervised learning algorithms experience a dataset containing many features, then learn useful properties of the structure of this dataset. 
* Supervised learning algorithms experience a dataset containing features but each example is associated with a label or target y.  
* A way to describe a dataset is with a design matrix containing a different example in each row and each column of the matrix corresponds to a different feature. Most of the learning algorithms are described in terms of how they operate on design matrix datasets. 
* The term "Generalization" refers the ability of a learning algorithm to perform well on new unseen inputs. The generalization error is deﬁned as the expected value of the error on a new input and is estimate by measuring the machine learing model's performance on a test set of examples.
* The training and test data are generated by a probability distribution over datasets called the data-generating process. 
* the factors determining how well a machine learning algorithm will perform are its ability to : 1. Make the training error small. 2 : Make the gap between training and test error small.
* Underfitting : the model isn't able to obtain a sufficiently low error value on the training set. 
* Overfitting : the gap between the training and test error is loo large. 
* To control whether a model is more likely to overfit or underfit, we can altering its capacity (= its ability to fit a wide variety of functions) by choosing its hypothesis space (= a set of fcn that the algorithm is allowed to select as being the solution.
* MLA will perform better when their capacity is adapt for complex tasks and may overfit when their capacity is higher than needed.
* Difference between optimization and machine learning : for our MLA, we are not trying to find the best function but one that can significantly reduce the training error. The imperfection of the optimization algorithm means that the leraning algorithm's effective capacity may be less than the representational capacity of the model family (= the set of functions the MLA can choose)
* Regularization is any modiﬁcation we make to a learning algorithm that is intended to reduce its generalization error but not its training error.
* The no free lunch theorem : there is no best machine learning algorithm, and, in particular, no best form of regularization. Instead, we must choose a form of regularization that is well suited to the particular task we want to solve.

# 2020-04-16 #

* I was already familiar with Python. I've been programming on it for 2 years but programming a neural network is another level. 
* Still, I have understood almost everything about Tensorflow, thanks to the tutorial write-up. 
* In the beginning, I was afraid to not be able to understand but it's more or less okay now. 
* We would to identify the pattern. Convolutionnal neural network use a technique consisting of matching part of the image to the label, rather than the whole image at once : it's called filtering. 
* Pooling consists in reducing the image obtained after filtering 
* Normalization : changing all the negative values in zero. We than get RELUs. 

# 2020-04-18 #

* Today, I watched the video about how to prevent algorithm bias and I was pretty surprised of how "easy" it was for computers to imitate humans being, from images. This can lead to damages so we have to be aware of the dangers of AI and how to use it conscientiously 
* I've also been in touch with my teammates to know who participate to the project or not, but i'm thinking about doing the project alone since we don't have all the same background, same schedule, same motivation. It could be interesting to work with other people and to learn from them, but sinds it's already end of the year and I'm a very stressful person, I would prefer put the stress on my shoulders and go at my own pace. 
* I still have some questions about how to do the project and will ask them to the professor next time. 

# 2020-05-28 #

* Today I attended the class for the first time. We talked about our dream job 10 years from now and automation. About the dream job, I described my projects for the future : 
* First, I want to work in Africa, ideally in Ghana or Benin since I come from there. 
* My dream is to work in the transport field and precisely to develop railways in Africa since it's something totally missing on this continent and it's necessary for the rise of Africa. 
* I'm currently reading a book titled "rail infrastructure in Africa" written by the African Bank of Development, to increase my kwowledges about the present situation. 
* The technology I would like to bring is magnetic levitation. 
* Then I would also like to work in the energy field because Africa has all necessary resources (rivers, sun, wind) but still that only 40% of the African population has access to electricity.
* I want to be an entrepeneur. 

# Article summary #

* The objective of this article is to evaluate the performance of four Machine learning algorithms (Decision Tree, K-Nearest Neighbours, Artificial Neural, Network and Deep Neural Network) used to predict and diagnose diabetes. The algorithms are trained and tested on the Pima Indian dataset. 
* The dataset consists in 768 samples (divided in two parts : healthy and diabetic patients) with 8 features and one output (either 0: Not diabetic, either 1: Diabetic). The ratio used for training and testing process is 80:20.
* The steps of the research consist in a pre-processing of the data to reduce the dimension and select the most relevant features. Then we selct the best subset of features and train the algorithms to obtain the best models (based on the highest accuracy) before using them for the test data.
* The criteria used for our comparison of classification are accuracy, sensitivity and specificity.
* The outcome after using the test data to evaluate the quality of the pretrained model shows that  the DNN model achieves prediction more accurately than other three predictors models : so DNN has high capability of classifying diabetic disease.

