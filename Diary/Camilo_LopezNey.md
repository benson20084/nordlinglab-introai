This diary file is written by Camilo López F04077142 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-20 #

## LECTURE 1 ##

- Bitbucket interface is not intuitive for me*
- I Learned a lot about the different fields in which A.I. has been implemented*
- It is really interesting what we can accomplish with the use of A.I.*

# 2020-03-21 #

## LECTURE 2##

### How should we define A.I.?###
- There is not an “absolute” A.I. (that can perform anything) *
- A.I. is a discipline rather than a “Thing” *
- Autonomy and Adaptivity are two important characteristics to define an A.I*
- Usually the most difficult task for humans are the easiest ones for A.I.´s and vice versa* 
- It is not correct to compare A.I´s that are trained to do different tasks, just as you would never compare an artist and an Engineer* 

### Related Fields###
- ”Data Science” includes Machine Learning and Statistics, which make use of A.I. *
- Robotics is the biggest challenge for A.I. because it involves using several of them at the same time in order to achieve something really complex*

### Philosophy of AI###
- ”The intelligent behavior of the system is fundamentally different from actually being intelligent”*
- The existence of a AGI seems far away from reality*
- There is not truly a STRONG A.I.*

# 2020-03-29 #

## LECTURE 3 ##

- No homework for this week.
- Maybe we should consider making this class a regular one and not an online course.
- Still waiting to learn something about Python.


# 2020-04-05 #

## LECTURE 4 ##

- No homework for this week.
- I though The professor was going to teach me python, but reading by myself the content of Python is not what I was expecting from this course.
- I am really disappointed, because I have not learn anything yet. 

# 2020-04-18 #

## LECTURE 6 ##
- We had our first synchronous online lecture
- We discussed about what problems are we having with the course and which challenges are the most difficult ones.
- I read  a few things about Python.Still not able to completely understand it.

# 2020-04-26 #

## LECTURE 7 ##
- Trying to catch up with the Programming part, Learning python has been hard for me, specially when I´m teaching myself everything.
- We had our second synchronous online "Lecture", but it is actually a Q&A of 2 hours.
- I would enjoy if the professror could actually teach me something. 
- I still have not started with Neural networks, I´m too busy with my important courses.
- A few changes were made, but I´m still not happy with the development of this course.

# 2020-05-03 #

## LECTURE 8 ##
- I saw the video of The visualisation of gradient descent 
- I saw the video of The visualisation of backpropagation
-  The videos are really interesting, and learn a lot of new concepts, but I do not know how to apply them. 

# 2020-05-24 #

## LECTURE 10 ##
- First physical course.
- The quality of the course improved a 100% because it was changed to a Physical lecture.
- Excited to work in the final project with my classmates.

# 2020-05-31 #

##Automation and the future of work##

- Automation will help point out the already existing inequality among individuals.
- Automation can also push the people to keep pursuing specialization in scientific fields.
- There is nothing wrong with automation itself, the actual problem is the lack of opportunities for people to pursue a degree in an academic field, which results in low-skill jobs.

# 2020-06-07 #

- We received our second physical lecture. And it was about automation.
- I comment on my diary about automation last week.
- This week apparently we finished our group project and uploaded the video to Youtube already.
- I enjoy and learn more during the physical lectures
- With automation, new jobs will appear, and some will disappear, so in some way there will be a balance, except these new jobs will require high specialization.

# 2020-06-14 #
- We Finished our group project, so I´m using my free time to review the material of this course.
- I still have to prepare myself for the Final.
- I believe the reason ML seems easy is because of all the tools used to do it, I mean TensorFlow and Keras, but if you do it from scratch, is probably really difficult.

