This diary file is written by Pauline Tsai E64066290 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20#

* Before the class starting, I'm really nervous.
* I am afraid that my English is not good enough.
* After the first class, maybe some fragments I can't understand immediately, but I'm able to follow up the class.
* I will try my best！

# 2019-02-27 #

* Good to learn something new today.
* I assume that artificial intelligence is to be able to think like a person.
* But I haven't know how complicated to do it.
* Today's class let me know partial of AI.
* However, Artificial intelligence covers a wide range of fields,to learn more about it,it is necessary to search more information from other place.

# 2019-03-06 #

* I have learned c++ before, therefore, it is not really difficult to me to understand Python.
* For learning, Python is more easier than c++ .
* Although Python and c++ have a lot in common.
* Python is a more convenient programming language.
* Because it simplifies how to write.

# 2019-03-13 #

* Repeated practice is very helpful for writing programs.
* Because we can become familiar with the functions in the shortest time.
* In addition, we can also learn more about the use of symbols.
* I assume that symbol is the most difficult part.
* Sometimes, just one more semicolon will have completely different results.

# 2019-03-20 #

* Review today's class, I fill a little bit scared.
* Our thinking behavior is divided into three modes,reinforcement learning,supervised learning,unsupervised learning.
* AI also qualified these three kinds abilities. However, just part of them.
* Like AlphaGo, the example in today's class, it just has the ability of reinforcement learning and playing chess.
* What if somedays, Al qualified all these three kinds abilities, and is able to do more things.
* Then it will be more similar to people.
* Even more stronger than people.
* This fact makes me a little bit sacred.

# 2019-03-27 #

* Big data and neuraul network let me recall a movie I have saw before. It talks about privacy issues.
* People all have some secrets, something want no one know. Then if the dig data begin to intervene in our life. Privacy would be the biggest problem.
* When we desire to get somthing, we must lost something in the same time. There is no free lunch in the world.

* And talk about the question why all the models are wrong.
* Initially, in my opinion, I think it is because there are too many factors, but no model is able to deal wuth all of them.
* In engineering, If there is a model can solve anything, I would assume it is rightand perfect. 
* I never relize that every model must be wrong.
* It is a excellent idea, is the best answer, too.
 
# 2019-04-10 #
* Today's class was devided in two parts, first part is gradient decent, the other part is convolutional neural networks.
* I thought that the most important part in the gradient decent is to find the key part of the function so that we won't be influence by the noise datas.
* The second part is about simplify and analysis, by using these methods we can get the maximum, minimum, or average of the datas more effectively.

# 2019-04-17 #
* It is really excited to learn tensor flow.
* As long as I am familiar enough with phython, I can write the program of layer.
* Then, I am able to create a simple neuraul network.
* However, it couldn't make me think that neuraul network is easy to create.
* This kind of neuraul network can only recognize zero to ten.
* Want it recognize other things, we need lots of deta and modify partial program first.
* In addition, enough time and a group of engineers is necessary.
* Because we need training and correcting  and training......
* Therefore, tensor flow let me know more about neuraul network,but also cosider that neuraul network is quite complicated.

# 2019-04-24 #
* I felt difficult initially, caucse there were several parts I couldn't realize.
* But if I ingored these pieces, I was able to guess the meaning.
* Alough with google and examples,it costs me some time to edit the code to satisfy the requestions.
* In additions, I thought it didn't mean that I understand the code.
* I must google more details after the class.

# 2019-05-01 #
* After today's discussion, I still thought deep learning is difficult.
* But it is not mean that it is difficult to finish the programming.
* During I tried to finish the exercise, I found several websites introduse the details of the programming, and the introduction is really difficult.
* For example, I googled the word, optimizer, and the website wrote detailly about lots of kinds of optimizers and there pros and cons.
* How impossible to wcreate the code of the optimizer! It really surprised me.
* And when I googled the "epoch", I felt surprised again.
* The bifficult part is how to create the code, the programming to construct the neuraul network.
* Therefore, I finish the exercise, but I felt I just learned a little bit!

# 2019-05-08 #
* Reviewed some concepts from the previous class, also emphasize some important parts that we should work on.
* Explained more about how to do the works by answering other's question, which makes it more clearly.
* Practice makes perfect.

# 2019-05-15 #
* Some people say that the Forth industrial revolution is based on AI participate in most of the production process.
* Industrial revolution can be judge by some factors: 1. Machine power 2. Electric power 3. Information green power. 
* If they are different, means another revolution is coming.

# 2019-05-22 #
* Algorithm bias does not mean fair. And when the datas are enough, it will merely close to fair.
* When the datas are occupied by negative examples, the outcomes are negative, either. This phenomenon can be saw at youtube searching, Microsoft's AI, Tay, and so on.
* And talk about fake video of real people, it is  horrible enough by imaging.
* Leaders of coutries, internet celebrities,any one of them is posted fake video with dangerous speech, must cause destructive consequence.
* In addition, the worse truth is that it is really difficult to distinguish the video is real or not!

# 2019-05-29 #
* A little bit nervous to work with my group mates, cause we merely knew each other on the class, and we need to work together at the same time.
* And so glad that I have finished and understood what we should do before class, or it must be difficult to me to understand what my group mates said.
* My poor English ability make me shy to say somthing, hope it can beimproved by working with others.

# 2019-06-06 #
* It is really efficient to learn tansorflow by Changing the code and trying it again and again. 
* Training the model repeatly can improve the accuracy but I wonder it has limit or not, if the accuracy is low initially, can the accuracy be improved to maybe 0.99?
* We can change the "epoch number", like change to 20, then the model would runs 20 times, but it doesn't mean the more time we run, the higher accuracy we get, we need to try the best one.
* We can find code on the website and try their code, and find the best one, we cannot get the same results with the writer, cause our data is different with them.

# 2019-06-12 #
* After trying the code by myself, I do have some conclusion.
* First, I think the code has the limit, if the code isn't perfect enough, no matter how many time we trains it, it would have a limit.
* Second, we can add the functions convolution2D and Maxpooling2D and batchnormalization to the code, it can improves the accuracy, cause the functions would identificate the images simply.
* Third, choose relu+sigmoid to be the two hidden layer and the SGD to be the optimizer are the best combination. Conversely, relu+relu+adam is the worst comdination.
* Last but not least, change the number of the batch_size can save the time of code running, however, it may lower the value accuracy a little bit.
* I haven't think I can learn too much things in this class, cause I have 25 credits this semester, but I did learn much!
* It is a great class for the student who have some programming experence.

# 2019-06-19 #
* In my opinion,the time is too short to finish the exam. Because I can't read and understand the english too fast, when professor said the time was up, I still had more than 10 questions to do.
* I think I would need 20~25 minutes to finish the exam.
* In group presentation, we can learn more things about tansorflow,cause we find the different sourse to edit the code.
* It is great to use charts to compare all resut datas, making it easy to us to view their results quickly. 
