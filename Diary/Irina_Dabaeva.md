This diary file is written by Irina Dabaeva F74077138 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-11 #

* i've watched the introduction video and now i feel pretty excited about this course

# 2020-03-18 #

* i've read about AI applications
* found out some new information (for example, I had no idea that AI is used in web design. And it was a surprise to know about such application as Wix ADI)

# 2020-03-25 #

* i've learned some basic python stuff (dict, tuple, oop)
* and I practiced python
* I read again the article about AI. And nowadays we can see applications of AI in many fields (i.e self-driving cars, content recommendations). All these things definitely make our life easier but at the same time, there are some issues. for example AI system in the self-driving car doesn’t understand its environment, and it doesn’t know how to drive safely, in the way a human being knows. And this means that the intelligent behavior of the system is fundamentally different from actually being intelligent. 

# 2020-04-01 #

* i've watched a video about Neural Network and read about Machine Learning Basics. 
* the idea of the neural network is similar to the brain. In the video, the speaker talked about digit recognition. As i understood digits are represented as different components. And every layer of the network determines whether the given inputs match to these components. So we can combine pixels into edges, edges into patterns, patterns into a digit. 
* also, i practiced python

# 2020-04-08 #

* i watched a video about tensorflow.
* it seems like a very useful software.
* now i have a more clear understanding of ml model training

# 2020-04-15 #

* the class we had this week was more about the professor's background
* i've read write-ups about training the neural networks. In two words we provide ml algorithm a labeled dataset and then "feed" it with the unlabeled info
* i also practiced python and made something remotely resembles a voice assistant (https://github.com/F74077138/voice_assistant)

# 2020-04-22 #

* i watched the video about Convolutional networks

The convolutional neural network (CNN) is a specialized type of neural network model designed for working with 2-dimensional image data

**some points:**

- Every image can be represented as a 2-dimensional array with a bunch of numbers (1 and -1)
-  We match parts of the image (*filtering*). In the filtering, we:
 1. line up feature and image patch. 
 2. multiply each pixel by corresponding feature pixel. 
 3. add them up. 
 4. divide it by the total number of pixels in the feature.
-  *Convolution* is the repeated application of the filter/feature over and over again. In convolution, one image becomes a stack of filtered images
-  We shrink the image stack (this called *pooling*). In pooling, we pick an image size, walk our window through the filtered images, and from each window take the max value
-  The next trick is *normalization* when we change everything negative to zero
-  Deep stacking makes the image gets more filtered

# 2020-04-29 #

* i watched a video about gradient descent and backpropagation
* Gradient descent is something like an optimization algorithm that is used for minimizing function by moving in the direction of steepest descent. So we use gradient descent to update the parameters of the model. In two words we have to find the global minimum:
![](https://cdn-images-1.medium.com/max/1600/0*fU8XFt-NCMZGAWND.)
* Backpropagation is used to minimize the cost function by adjusting the network’s weights and biases. So it takes the error and uses this error to adjust the neural network’s parameters in the direction of less error

# 2020-05-06 #

* i wrote a classifier to recognize digits on the MNIST dataset (by watching a tutorial on youtube)
* (https://colab.research.google.com/drive/1CpIKDXzlKDslYTwTaVaiZ1EhcS7prpzS?authuser=2#scrollTo=mQk9IvqZ98vD)

# 2020-05-13 #

* i watched TED videos about the dangers of algorithm bias
* people pass their vision of the world to the AI model. But what if a person has the prejudices against some minority. So opinions of society can affect the work of AI

# 2020-05-20 #

* this week we had a discussion related to ethics and the danger of algorithm bias. It was really interesting to know others' opinions
* before the lecture i finished watching videos professor recommended us
* i don't think making AGI is possible now. First of all, because it will learn from us how to behave in society. But most people are racist, homophobic and etc. So it will see that people just hate each other and it will think that it's absolutely normal

# 2020-05-27 #

* this week i had a meeting with my teammates. We discussed the final project

# 2020-06-03 #

* today we talked about how automation will affect future jobs. And the thing is machines and robots can replace a huge percentage of the world’s current human labor. But there will be an increasing number of jobs related to programming, engineering, etc.
* i'd like to have a job related to programming so it more likely i'm not gonna become unemployed in the future. In my opinion, education is the key to overcome automation anxiety

# 2020-06-10 #

* this week i had a discussion with teammates 