  This diary file is written by Po-Lun,Chen F84061113 in the course "Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists".
# 2020-03-22 #

## Lecture 2:What is AI, Deep Learning, and Machine Learning?## 
### How should we define AI? ###

* There is no absolute definition for AI
* The useful definition of AI is autonomy and adaptivity because we expect AI can improve themselves by learning from experience without constant guidance by a user.

### Related fields ###
* In addition to AI, there are several other closely related topics, like machine learning, data science, and deep learning.
* Data science includes machine learning and statistics.
* In a way, robotics requires a combination of virtually all areas of AI. 
* Machine learning is important since it is a good approach to solve some AI problems.

### Philosophy of AI ###
* Consciousness is replicable by computing 
* Turing test is a test for intelligence to measure the behavior of computer like a human or not.
* The intelligent behavior of the system is fundamentally different from actually being intelligent.

# 2020-03-29 #
* I take autonomous learning this week. Because I have some experience on coding, I try to write some code. 
* There are some code I reviewed today.
    1.Web scrapying
    2.reading wnd writing excel files
    3.Run code at a specific time 
* Finally, I conbine three different thing together. I write a code which can get the information from internet and input the data in excel automatically

# 2020-05-16 #
* I learn the material which I lose last two week.

* For a system, some information is important and some information is not. So, we can control weights and bias to decide which patterns filters detect.   

# 2020-05-17 #
* I review some material which I learn in calss to prepare the group project.

# 2020-05-20 #
* It is the first physical iecture, and we discuss ethics and the danger of algorithm bias.
* I discuss those question with my classmate ,and their thoughts are quite surprised.
* Finally,I still confuse on the question "How to create a society where we treat each other fairly?".
* For me, I cannot imagine a world which stands for fairly. If we want to create a relatively equal world, it means that we need a lot of rules to measure the resource which people get. The amount of rule is unlimited.

# 2020-05-31 #
* Today, I finish the group project. I use a convolutional neural network model to recognize the number.
* The error rate is 0.95%. It gives me a lot of confidence.
* So, I decide to create a model to recognize English alphabet.

# 2020-06-07 #
* Although automation may bring a big impact in the future, I believe the world will strike a balance.
* Just like the Industrial Revolution, althought the machines replaced a lot of people, it also liberated the workforce and enabled people to create more things in other fields.
* For me, I think automation helps me a lot in the future.I believe that I will work as a researcher and automation can help me to establish new standard operating procedure.
