# article summaries 
*It is known that the learning rate is the most important hyper-parameter to tune for training deep neural networks. 
*This paper describes a new method for setting the learning rate, named cyclical learning rates, which practically eliminates the need to experimentally find the best values and schedule for the global learning rates. 
*Instead of monotonically decreasing the learning rate, this method lets the learning rate cyclically vary between reasonable boundary values. 
*Training with cyclical learning rates instead of fixed values achieves improved classification accuracy without a need to tune and often in fewer iterations. 
*This paper also describes a simple way to estimate “reasonable bounds” – linearly increasing the learning rate of the network for a few epochs. 
*In addition, cyclical learning rates are demonstrated on the CIFAR-10 and CIFAR-100 datasets with ResNets, Stochastic Depth networks, and DenseNets, and the ImageNet dataset with the AlexNet and GoogLeNet architectures. 
*These are practical tools for everyone who trains neural networks.

# 2020-05-28 #
*model = Sequential()

*model.add(Conv2D(32, (5,5), activation="relu", padding="same", data_format="channels_last", input_shape=(28,28,1)))
*model.add(Conv2D(32, (5,5), activation="relu", padding="same", data_format="channels_last", input_shape=(28,28,1)))
*model.add(MaxPooling2D(pool_size=(2,2), data_format="channels_last"))
*model.add(Dropout(0.25))

*model.add(Conv2D(64, (3,3), activation="relu", padding="same", data_format="channels_last"))
*model.add(Conv2D(64, (3,3), activation="relu", padding="same", data_format="channels_last"))
*model.add(MaxPooling2D(pool_size=(2,2), data_format="channels_last"))
*model.add(Dropout(0.25))

*model.add(Flatten())
*model.add(Dense(256, activation="relu"))
*model.add(Dropout(0.5))
*model.add(Dense(10, activation="softmax"))

# 2020-05-21 #
*y_test_onehot = np_utils.to_categorical(y_test)
*score = model.evaluate(x_test, y_test_onehot)
*print()
*print("Accuracy {}%".format(score[1]))

# 2020-05-14 #
*This week, discuss with the team members how to start the final report.

# 2020-05-07 #
*This week, the partners in the same group were confirmed, hoping to learn together and deliver good results.

# 2020-04-30 #
*Commenting on a cell 
You can comment on a Colaboratory notebook like you would on a Google Document. Comments are attached to cells, and are displayed next to the cell they refer to. 
If you have comment-only permissions, you will see a comment button on the top right of the cell when you hover over it.

# 2020-04-23 #
*Try to output the number to chart format.
*Rich, interactive outputs
import numpy as np
from matplotlib import pyplot as plt

ys = 200 + np.random.randn(100)
x = [x for x in range(len(ys))]

plt.plot(x, ys, '-')
plt.fill_between(x, ys, 195, where=(ys > 195), facecolor='g', alpha=0.6)

plt.title("Fills and Alpha Example")
plt.show()

# 2020-04-16 #
*Today I tried to use the basic operations of Python. According to the teaching, I can write the expected results, but there are too many types of program codes and it takes a little time to digest.

# 2020-04-09 #
*Read 5.1 Learning Algorithms，From a scienti ﬁ c andphilosophical point of view, machine learning is interesting because developing our understanding of it entails developing our understanding of the principles thatunderlie intelligence.
*The use of coding makes me very unfamiliar, I hope that the exam does not need to be too deep.

# 2020-04-02 #
*In Industry 4.0, the most common use of neural network-like way to establish a database, complete the basic statistical induction method, provide a processor for discrimination.


# 2020-03-26 #
*Read the introduction to Python. The high-level programming language of transliteration is already a very convenient modern programming language. 
*I hope to use simple applications in this class.

# 2020-03-19 #
*For the era of Industry 4.0, I think we need a course like this.
*In addition, because I did not learn procedural language, I hope the course is not too difficult.


This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Martin Wu E1000000 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-02-20 #

* The first lecture was great.
* I wish to know more about ImageNet.
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* I don't think exponential growth applies to food production.
* Why doesn't my doctor use an AI.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2019-02-27 #

* The second lecture was a little borring because I knew Python.
* I wish the Python exercise was a bit more challenging.
* I learnt the difference of `print` in Python version 2 and 3.
* I love the [Learn Python the hard way](https://learnpythonthehardway.org/) book and recommend it to everyone. 
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is helpful.
* Now I understood that I should write the diary entry for all weeks in this same file.