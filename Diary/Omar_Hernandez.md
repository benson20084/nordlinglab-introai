*This diary file is written by Omar Hernandez (E24057031) in the course of Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.
																	*(Spring 2020).
                                                                    
                                                                    
# 2020-03-05

## 1st Lecture
* No lecture.
* Class was changed to an online version due to COVID-19.

> Omar Hernandez

                                                                  
# 2020-03-12

## 2nd Lecture
* No lecture.
* Youtube course intro video posted.

> Omar Hernandez

                                                                    
# 2020-03-19

## 3rd Lecture
* AI Success Story: Voice Recreation for ALS patient by Google and DeepMind.
* I think it so amazingly unbelievable how AI is making things possible.
* Things we never imagined could happen.
* Tim Shaw was able to hear his voice read a letter he had written to his younger self while he already was diagnosed with ALS.
* The model was also able to display the words he said, using speech recognition.
* Using a lot of sample phrases of his voice while he was not ill. 
* Given the short time, the model was able to successfully display and output the sound of his voice. 
* It is however, not perfect.
* More research is being done to improve it.
* Researchers would like to make this model available into many other languages. 
* I can't believe how incredible our technology is today and I can only imagine all the future possibilities.
* I'm amazed by AI!

> Omar Hernandez

# 2020-03-26

## 4th Lecture
* Very eye-opening article.
* I always assumed AI's definition was universally and officially agreed upon. Seems some people still are not sure.
* What is AI and what is not AI is a hard question.
* AI's definintion is very unclear for the public for 3 main reason:
    * No officially agreed definition
    * Science fiction may give us a bad idea of what AI "looks" like
    * It is hard to know which things are hard and which are easy
* Aplications of AI:
    * Self-driving cars
    * Content recommendation
    * Image and video processing
* A more useful definition of AI contains the key words: Autonomy and Adaptivity
* That is, AI would be capable to perform tasks in complex enviroments (automomy) without guidance and
* Improve based on experience (adaptivity).
* Don't say "an AI"; say "a pinch of AI".
* Not a countable noun.
* Deep learning, machine learning, data science and robotics are all related fields of AI.
* Machine learning is a subfield of AI, which itself is a subfield of computer science.
* Deep learning is a subfield of machine learning.
* Data science includes machine learning and statistics and some aspects of computer science.
* Robotics is building and programming robots so they can operate in complex, real-world scenarios.
* Robotics can by seen as the ultimate challenge of AI as a combination of virtually all areas of AI are required. 
* The topic of AI raises some philosophical questions.

> Omar Hernandez

# 2020-04-02

## 5th Lecture
* Spring break.
* Just quickly skimmed through material for this week.
* Became aware I need to learn how to program in Python.

> Omar Hernandez


# 2020-04-09

## 6th Lecture
* Researched a bit about why Python is preferred for Machine Learning.
* Rich library.
* Easy to program
* "Closest to English language programming language".


> Omar Hernandez

# 2020-04-16

## 7th Lecture
* Started learning Python basics.
* Seems way simpler to program syntax-wise compared to Java, C++ or C.
* Was having a bit of a problem with indentation problems while programming.
* Not sure if Python is indentation senstive.
* Meaning if an indentation not matching with one above won't allow my code to compile.
* Need to practice more with Python.
* I am discovering I actually enjoy programming even though I don't have a lot of programming experience. 
* Looking forward to getting better at Python.

> Omar Hernandez



# 2020-04-23

## 8th Lecture
* My birthday was yesterday!
* Continued practicing Python basics. 
* Practiced Variables and Functins today. 
* Realized that defining a function in Python is much easier than C++.
* Some more practice on if, range for, and while loop functions.


> Omar Hernandez


# 2020-04-30

## 9th Lecture
* Began learning Dictionaries in Python.
* Went over inheritance. 
* I understand the concept but the example code given I think is a bit too complex for me to understand the syntax. 
* Was fun creating my own dictionary. 
* Was able to print different fiels from my dictionary. 

> Omar Hernandez



# 2020-05-07

## 10th Lecture
* Practiced reading and writing files.
* So cool!
* Learned how to create a blank .txt file, write something on it and then read it. 
* Learned how to add a new line to an existing .txt file.
* Went over Error Handling. 
* Have to be careful with syntax errors to avoid making simple mistakes. 
* Will try to finish up Tensor Flow this week! 
* Have to catch up THIS week! Let's go! 

> Omar Hernandez


# 2020-05-14

## 11th Lecture
* In-class lecture.
* We watched videos on the ethics of AI.
* Very interesting talks. 
* Made me ponder on the question: What decisions do we want to make and what decisions do we want AI to make?
* Had small group discussions about biased algorithms. 
* Met with some of my group members. 

> Omar Hernandez


# 2020-05-21

## 12th Lecture
* In this week I was learning Neural Networks.
* I'm mindblown!
* The simple problem of a computer being able to recognize digits is a very challenging and daunting one.
* I confirmed my concept of neural network. It being some sort of interconnected "neurons" just like our brain has.
* It is very surprising to learn that a neural network's nuerons get "activated" by the previous layer.
* Breaking down the problem into small chunks represented as a mathematical formula is complex yet brilliant. 
* I did not know a bias number is needed to give it sense to the result. 
* When we talk about "learning" is nothing more than the computer finding the right weights and biases. 
* I assumed AI would involve calculus, but it seems all comes back to understanding linear algebra.
* That was surprsing to me. 
* Amazed by the fact that when coding the "digit recgonition" problem the formula can easily be expressed in a simple way.
* Sigmoid is basically a function that squishes the result between 0 and 1. 

> Omar Hernandez



# 2020-05-28

## 13th Lecture
* Today we talked about our dream job.
* My dream job is to be and entreprenuer in the Electrical Engineering field. 
* I want to be my own boss. 
* I learned that some of my classmates still want to have ta permanent employment and work for someone. 
* We also talked about the 3rd industrial revolution. 
* A revolution is "defined" by the way it changes or alters how we communicate, move and power our society.
* According to professor, 4th industrial revolution has been going on from long time ago, but it has never fully been a complete one.
* It was striking to understand Dr. Nordling's point of view as it does make sense why we still are in the 3rd industrial revolution.
* This is contrary to what I had learned in my Introduction to Computers class. 
* It was also eye-opening to learn that in the next 5 years electric cars will be cheaper than combustion vehicles. 
* I want to get a Tesla car for myself in the future.
* I want to experience an electric car. 
* We also began our dicussion about how AI can affect our future jobs.
* Automation of jobs is a very important topic we as future engineers need to consider in choosing our career path in the future.

> Omar Hernandez



# 2020-06-04

## 14th Lecture
* Today I learned about Tensor Flow.
* It's an open source Machine Learning tool invented by Google. 
* It makes it easy and simple for Machine Learning to occur. 
* You can train the computer to recognize handwritten digits.
* After training the computer can recognize the digits with very high accuracies.
* It is integrated into the Python toolbox.
* Anyone without a PhD and just interested in learning about Machine Learning can learn how to use Tensor Flow.
* I'm super excited to be able to train my model on my own and hopefully I can get high accuracies.
* I think I am really getting interesting in Artificial Intelligence and maybe I pursue my Master's Degree in this.
* I would like to apply AI to electric vehicles and help, for example Tesla, to put safe autonomous vehicles on the roads.

> Omar Hernandez



# 2020-06-11

## 15th Lecture
* Today we had article summary presentations.
* I learned quite a few things from my classmate's presentation.
* One classmate mentioned her article talked about training times taking up to 5 days to complete but only milliseconds to make a computation.
* Today's presentation made a bit more clear the difference between supervised and unsupervised Machine Learning.
* Supervised learning is used for classfication purposes and the model is fed labeled data.
* Unsupervised learning is used for clustering purposes, that is, finding patterns and groupings from unlabeled data.

> Omar Hernandez

