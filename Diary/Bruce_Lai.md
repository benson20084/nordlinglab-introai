4/8
Finally starting this course for the first time due to my buggy computer,
hopefully I don't have to send it back for more bug fixing.
Can't wait to begin my studies on python, this is going to be fun.
Noticed that the success stories deadline is 5/31, so I'm going to have to make up for that.
I wonder if this is where I write my diary, could the teacher send me an email for confirmation?
Thanks! (brucelai881217@gmail.com)

5/13
Today is the first day I join the online classes. 
I've only truly started studying this course this past week, and the amount
of material is staggering.
I feel like there is a high chance I am going to fail,
but I want to give it one last shot.

5/22
This week we had our first physical class. I like it a lot, it was a lot of fun.
I've finished up lesson 5 this week, and moving on to lesson six. Hopefully, next
time I write this diary, I would have wrapped up lesson seven.
I met up with my teammates and was chosen as the leader. We've agreed to meet up next week.