# 2020-03-20 #

* I saw the welcome video and I would like to point out that the sound quality is not the best, otherwise it is alright.
* I am a bit confused about this course at the moment but I understand current conditions and believe that hard work is being done. 
* I was surprised(nice surprise) by [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2020-03-26 #

* I read the article https://course.elementsofai.com/1 which was after a few years of studing cybernetics kind of boring but this course is aimed generally so whatever.
* I am still very confused about this course - where and when are the video lecuter going to appear? I suppose on YouTube but when ...
* I revised my Python knowledges and can not wait for further progress with deep learning in this course.
* I collected all the information resources mentioned in this course.

# 2020-03-30 -> 2020-04-05 (holidays)#
* I am still very confused about this course.
* I started reading Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
* I started watching https://www.youtube.com/watch?v=ILsA4nyG7I0&list=PLVZqlMpoM6kaJX_2lLKjEhWI0NlqHfqzp
* I sterted watching 3Blue1Brown's Neural Networks: https://www.youtube.com/watch?v=aircAruvnKk&list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi

# 2020-04-12 #
* I am still very confused about this course.
* I continue reading Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
* I continue watching https://www.youtube.com/watch?v=ILsA4nyG7I0&list=PLVZqlMpoM6kaJX_2lLKjEhWI0NlqHfqzp
* I watched Martin Görner’s "TensorFlow and Deep Learning without a PhD".

# 2020-04-19 #
* I am still very confused about this course.
* I continue reading Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
* I play with TensorFLow.
* I was dissapointed with the last lecture (it was the very first lecture after 7 weeks and we just received general information about the course and the fact that the online lectures are not going to be availible by the next week)

# 2020-04-20 #
* I am still very confused about this course.
* I continue reading Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
* I play with TensorFLow.

# 2020-05-17 #
* I stopped watching lectures.
* I built a few CNN with tensorflow and google colab. I am currently comparing how different topologies affect performance.
* Keep learninng TensorFlow.
* I continue reading Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning.

# 2020-05-31 #
* I built a few CNN with tensorflow and google colab. I am currently comparing how different topologies affect performance.
* Keep learninng TensorFlow.
* I continue reading Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning.
* I spent a few nice minutes thinking about what to write in my diary when I finished my CNN while resting on the Orchid Island. 