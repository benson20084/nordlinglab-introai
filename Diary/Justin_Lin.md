---

# ___I'VE BEEN TOO BUSY, SO I HAVE DROPPED THIS COURSE___

---

_This diary file is written by **Justin Lin E24086129** in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists._

---
# _2020-03-17 Before class_ #
- Looking forward to learn AI and DL.
- I have learnt Python a little in this winter, that I wish to utilized it to develop artificial intelligence.
- I wish to know more about Python.
- I am trying to be familiar with [markdown syntax](https://www.markdownguide.org/basic-syntax/).
---
# _2020-03-18 Topic 1_ #
- I've watched the welcome video.
- I've uploaded the success story of AI  
      - I wrote something about how AI combines IoT.
      - I also saw other examples on cloud which impressed me that AI can have so many implementations.
- I watched [AlphaGo - The Movie | Full Documentary](http://youtube.com/watch?v=WXuK6gekU1Y&feature=youtu.be)
      - I am really impressed by the AI and the algorithm they done for Playing Go.
      - AlphaGo is certainly amazing, but I don't think AI will defeat human one day, as we are the one who create
        it. In the documentary, although AlphaGo defeat Lee Sedol four times, but there's still have the only
        time that Lee Sedol defeat AI. Therefore, I think that human brain is still outperform AI to some extent.
      - There would be many moves in Go, so using probability in AlphaGo is really a good idea.
---
# _2020-03-27 Topic 2_ #
- I've read the [Introduction to AI](https://course.elementsofai.com/1), 
  the following is my reviews and thoughts about it.
      1. **How should we define AI?**  
        It recall to me that my teacher of introduction to computers once said that, AI for now is more like 
        "augmented intellengence", since it only help human to deal with datas, but not actually "thinking".
        But in the article, I learnt that AI have more aspects than that, and the definition of AI also comes 
        in different ways. But the whole picture are "autonomy" and "adaptivity". 
      2. **Related fields**  
        In this article, I learnt some of the related field of AI, such as machine learning, deep learning, 
        data science and robotics. I was really confuse by the term "machine learning" and "deep learning", 
        because it seems to be the same thing!
      3. **Philosophy of AI**  
        I don't think one day a robot can become a human, whether how "intellegent" it is. Since that robot won't 
        have emotions(it would be simulation if it does have). Another topic of philosophy of AI I think of is 
        "Trolley problem" that AI would encouter. For example, when a self-driving car face the dilemma of whether craching 
        into 5 people or getting aside but kill 1 person, what would it do?     
---
# _2020-04-10 Topic 3_ #
- The topic of the third week is _Introduction to Python I._
- The professor has not upload the videos yet, so I tried to find some Python-learning resources 
  like: [Python tutorial](https://www.youtube.com/playlist?list=PL-g0fdC5RMboYEyt6QS2iLb_1m7QcgfHk) and 
  [Python Tutorial for Beginners](https://www.youtube.com/watch?v=_uQrJ0TkZlc).
- The following are what I've learnt:
	- **Comment**  
        - For human to see, help you understand what the codes are doing.  
        - It come in handy when you are debugging (can erase part of the code not by deleting it).
        - Here is an example:  
			`# Comments always start with a hash`  
			`# Computer won't read this`  
        
	- **Basic output**  
	  	- Can print string, integer, float...and so much more on the terminal by using it.  
		- Here is an example:  
			`print("Hello world!")`  
			`str = "Helllo world!"`  
			`print(str)`  
	- **Basic input**
		- Can get data from the user (by the terminal).
		- Here is an example:  
			`a = int(input("Enter an integer: "))`  
			This code means that it will print `"Enter an interger: "` on terminal, and read the input 
			given by the user. finally, converts the input into an integer.
	- **Conditional statements**
		- Determines what you want to do by giving a boolean equation.
		- Here is an example:  
			`if a > b:`  
				`statement...`
		- It should exist tabs in front of statements under "if".
	- **Loops**
		- While loop   
			`a = 0	# initialize`  
			`while a < 3:  # a boolean equation after while`  
					`print(a)  # statement`  
					`a += 1  #adjustment`  

		- For loop  
			`for i in range(5)`  
			`print(i)`  
		- It should exist tabs in front the block  
		
---
# _2020-04-15 Topic 4_ #
- The professor said that it will be a synchronous online lecture today in the morning, that I totally 
  missed the message and didn't go online to join the conference. I were really frustrated by this class 
  and how it is operated.
- The topic of this week is _Introduction to Python II_.
- The following are what I've learnt:
	- **Functions**
		- Define a function by using the `def` command.
		- Functions are used for performing specific tasks.
		- Using the concept of *"devide and conquer"*.
		- Here is an example:  
			`def Triple(x):`  
  			 _(tabs)_`return 3*x`  
	- **Variables**
		- Is a storage location in memory that holds a value and is identified by the variable name.
		- Not like C/C++, don't need to initialize by data types.
	- **Dictionaries**
		- Composite data type in Python.
		- Consist of _"key-value pair"_.
		- Here is an example:  
	- **Classes and inheritance**
		- In object oriented programming a class can inherit functions and variables from another class (data type).
	- **Module**
		- A module is a file containing Python definitions and statements.
		- A package is a collection of modules enabling use of dot notation.
		- Are sepatated file.
	- **File handling**
		- Create a .txt file and write some text in it.
		- The "w" indicates "write" and the plus sign means that we create the file if it does not already exist. 
		- Open the file again for reading. The "r" indicates "read".
- Coding practice for today (dictionary) 
```python
		thisdict =	{  
  			"Name": ["Amanda","Mark"],  
  			"Job": ["Teacher","Doctor"],  
 		 	"Age": [23,48]  
		}  
```  
- Coding practice for today (file processing) 
```python
		f = open("Demofile.txt", "w+")
		f.write("This is some text we wrote in the file.")
		f.close() 
		f = open("Demofile.txt", "r")
		print(f.read())
		f.close()
```  
---
# _2020-04-23 Topic 5_ #
- The topic of the third week is _Introduction to Neural Networks and Modelling_.
- I've watch [But what is a Neural Network? | Deep learning, chapter 1](https://www.youtube.com/watch?v=aircAruvnKk&feature=youtu.be) 
	by 3blue1brown.
	- It helps me understand why it called _**neural network**_.
	- To train a model is actually finding a best solution of the combiations od sub-components.
	- I think that it is when linear algebra that I've just learned comes into use.








