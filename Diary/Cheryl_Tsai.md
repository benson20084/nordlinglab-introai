# 2020-03-05

* Due to the COVID-19 epidemic, we didnt have physical class but professor is trying to work this out!

# 2020-03-12

* Professor uploaded a welcome video on Youtube.
* We also watched some videos.
* At the end of the day, I filled out some form about this course.

# 2020-03-18

* I acutally hope that we have class in person because I like to attend the couse that the professor lectured.
* I have spent a lot of time figuring out how this course works because we don't acutally have physical class so actually it is really hard to understand everything on my own.
* I love the contents of the video for this time which I have learned a lot of information

# 2020-03-25

* I fonud out that it is acutally hard for me to understand this courses but I am still trying my best.
* I love the contents about this week.
* Really hope that we can soon have the physical classes!Because I really wanna learn more stuff with the classmates.

# 2020-04-01

* Checked some articles about AI today and done one of the successful stories and also uploaded on the google drive.
* I feel rally productive today since that I always spend a lot of time understand a article. However, I finished the articles really quick today.
* I think it's acutally getting easier to understand this course and also know how to finish the hw and things on time.

# 2020-04-08

* I finished a success story yesterday which is productive for me.
* I'm really excited about the upcoming videos about this lecture.
* I hope that I can finish one more success story before this Friday!
* Last but not least, I feel like I have understood more about the Artificial Intelligence on Siri and Satellite.

# 2020-04-15

* I didn't know that we will have online course today till 13:30.
* However, I can surely know that we will have online course next week.
* This week I have finished one success story which I feel really productive.
* Hoping that we will learn more contents next week!

# 2020-04-22

* Today, I finally got to the online course and solved all the problems at the end of the class.
* I'm extremely happy about the course because the professor taught us how to do the computer program.
* Although I can't really understand all of the stuff that the professor taught us, I will still work this on my own and find the anwsers via different media.
* Glad that I solved the problems that I have for the previous weeks.

# 2020-04-29

* To be honest, the course for today is actually hard to understand but I think I'll check out the course from Readme and see if this can get better.
* Also, I figure out that the success story only needs to finish one. However, I have done two so I feel like I have gained more knowledge which I am happy about this.

# 2020-05-06

* Today's course was about program and I feel like it was a bit confusing because it contained a large number of content.
* I am excited about the group for the coming week!

# 2020-05-13

* Today's course was about announcing the group memeber and I found my group members during the class.
* Our group member has started the group chat and also the poll to see where to disscuss the presentation.
* For now, I am actually nervous about the final examination becuase this course is kind of hard to unerstand every contents.
* Hopefully, we can finish the presentation next week!

# 2020-05-20

* I'm really glad that we have physical class today! sisnce that we got the opportunities to know new classmates and the group member!
* However, we won't have physical class because it is for group discussion.

# 2020-05-27

* We don't have physical class but to discuss about the final presentation.
* The topic is actually hard for me since that this field is totally different from my major which is Foreign Language and Literature.
* I am really glad that we have group members that are familiar to this topic!

# 2020-06-03

* Today's lecture is about the automation.
* During class, we discuss about whether AI will affect our future job which is a interesting topic.
* I feel like my future job won't really affect by automation because I want to be a secretary, this job needs people to know others emotion and feeling.
* Therefore, I dont feel like the automation will affect to my future job.

# 2020-06-10

* We don't have physical class today but we have done the group discussion.
* I am glad that we have enough time to do the discussion of the presentation and also assign the part for each members.
* The only thing that I worry the most is the final exam. Since that this is totally differnt from what I learn in my major, this is actually tough for me to understand everything on my own without physical class. 
* However, I definitely will try my best to prepare the final exam!
