This diary file is written by Reinald Adrian Pugoy (李仁傑) for the course "Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists".

# Week 1: 2020-03-22  #

* For the first week, I am acclimatizing to the online, virtual setup of this course, which is an excellent idea.
* I am excited that this course will officially formalize my training in Deep Learning and Neural Networks, as I've learned most about them through self-learning and in our lab.
* I also am looking forward to getting my hands dirty with TensorFlow. My initial impression with TensorFlow is that it is inconvenient and cumbersome to utilize, and I would rather choose PyTorch. Yet, I am positive that this course will adequately guide me in (finally) using TensorFlow.
* For the first task (i.e., looking for an AI success story), thoroughly utilizing AI/Deep Learning is a great tool for automated essay scoring. Zaidi (2016) relied on the power of RNN and LSTM to accomplish the said task. (More details on the article summary below.)

# Week 2: 2020-03-28 #

* Indeed, there is a never-ending discussion about intelligence versus intelligent behaviour. Sure, Alan Turing, the father of Computer Science, has formulated a convincing theorem. On the other hand, John Searle also made an equally compelling argument. I agree that no one can give a definitive answer on this. Hence, I would say that each person has a different take on this discussion. This depends on the person's own philosophy and belief systems.
* During this age of rapid technological advancements which involve AI, people actually do not understand what AI is. Buzz words and suitcase words are just being thrown around. 
* I also appreciate how the article discusses the difference among Machine Learning, Deep Learning, and Data Science as people can easily confuse them with one another.

# Week 3: 2020-04-01 #

* I took the liberty to proceed to Topic #5 (Introduction to Neural Networks and Modelling) as I have experience in Python before.
* The video (i.e., the 3blue1brown visualization on neural networks) is one of the best and simplest videos I've encountered that discusses neural networks. 
* Basically, a neural network architecture is a "network of functions". For it to be intelligent (and for it to function like a human brain), it has to be composed of a complex combination of layers and sub-components. Moreover, it helps that it is complicated; if it is too simple, we will not believe its results.
* I used to think of it as a black-box. Well, it really is a black-box. But, it does not hurt to play with the weights on my own (just to get better insights on what the neural network is doing), which is something encouraged in the video.

# Week 4: 2020-04-12 #

* The Deep Learning reference by Goodfellow et al. actually does a great job in discussing deep learning concepts in simpler terms. Example: their definition of bias. Given bias b, the model will be "biased" toward b in the absence of any input.
* They also explained underfitting and overfitting, and how they should be dealt with. It is indeed true that a machine learning algorithm is not necessarily choosing the best functions or sets of functions, which is extremely tricky and challenging. The general sense is in finding certain parameters (hyperparameters to be specific) that can significantly reduce training error, whether it is in terms of MSE, RMSE, MAE, etc.
* Hence, regularization techniques such as weight decay can control the extent of underfitting or overfitting - a trade-off between fitting and smaller weights.
* In summary, machine learning is primarily composed of three essential concepts: optimization, generalization, and regularization.

# Week 5: 2020-04-19 #

* I would say that the presentation and materials of Martin Görner are really helpful for both beginners and seasoned developers alike.
* "The deeper, the better?" This is not always true. And, I suddenly remember Occam's Razor.
* It is important to monitor the training accuracy/loss and validation accuracy/loss for every epoch. It will help a lot if we can plot these for better visualization and analysis because we always have to be on the lookout for any possible cases of overfitting. The good thing is that there are a couple of Python libraries that can do this. 
* If the model tends to overfit, then an overfitting-mitigating strategy, such as weight decay (learning rate decay) or dropout, should be adopted and incorporated in the neural network model.

# Week 6: 2020-04-26 #

* The video by Brandon Rohrer provides an excellent illustration and explanation of Convolutional Neural Networks (CNN). He was able also to neatly explain the math behind convolutional filters.
* I can only imagine how CNN has revolutionized Computer Vision / Image Recognition / Image Analysis.
* Pooling (including max pooling) is less sensitive to positions (position-insensitive). Thus, it is useful to find a particular feature in a given image. 
* Normalization is basically "ReLU-ing" (application of the ReLU layer) to the features from the convolutional layers where negative values become 0, and the rest of the values are "identity".
* Moreover, we can stack the CNN layers and fully-connected layers for several times to get the best possible results (least errors). However, this should be handled with care and studied carefully, depending on the prediction task at hand. More layers do not automatically imply better performance. 

# Week 7: 2020-05-03 #

* I re-watched some of the previous videos made available in this course. I must say that the 3Blue1Brown video series has done a great job in simplifying deep learning and neural network concepts.
* Simplest or "layman" objective of neural networks: Finding the set of right weights and biases and also finding the minimum of a certain function.
* The cost function defines how "lousy" the neural network is.
* For anyone who wants to dive into NN, it is a must to have a basic knowledge of gradient descent and backpropagation.
* Finding the local minimum is feasible while discovering the global minimum is extremely and crazy difficult.

# Week 8: 2020-05-09 #

* I started to write my own code in Keras (albeit a simple one to test that everything is running well) using Martin Görner's Colab as a reference. I'm more comfortable running my code on my lab PC.
* Simultaneously, I tried to understand the format of the inputs (i.e., the images) that is gonna be used for the project.
* I experienced some minor difficulties as far as the versions and dependencies of Tensorflow and Keras are concerned.

# Week 9: 2020-05-17 #

* Attended the first-ever face-to-face class this semester, which is nice to have once in a while.
* Sure, AI has provided enormous benefits for the whole world to reap. However, the deep fake video is frightening. Specifically, its potential for misuse is ginormous and frightening.
* A possible application for its misuse would be politics considering the highly polarized political landscape in most of the countries.

# Week 10: 2020-05-23 #

* Initially, I encountered difficulties in running my Keras code on my lab PC. I also tried executing Martin Görner's original/unmodified codes from Collab but to no avail.
* I found out that there were version conflicts among TensorFlow, Keras, Jupyter, and other libraries. Let's say that a certain library requires a particular version for Keras. The versions have to be "perfectly matched" with each other. It's really cumbersome!
* This took a long time to solve and I'm glad that I got them figured out. I know that I can opt to simply use the Google Colab, yet I prefer to utilize my lab PC because this can hasten the code development process.

# Week 11: 2020-05-31 #

* Attended the second-ever face-to-face class, and the main topic is on automation and future of work.
* I would say that the topic is interesting and engaging. During this current era of AI advancements, humankind has found different uses for AI in virtually all domains and various tasks.
* This makes me look forward to the next class. I'm particularly interested in finding out which jobs are in danger in favor of AI.

# Week 12: 2020-06-07 #

* Still doing my project, and I'm almost done. 
* Using Keras is a lot easier compared to PyTorch. I would say that Keras is a great choice for quick prototyping
* Then, I gotta prepare my YouTube video for presenting my project.
* I am hoping to extract whatever creative juices I have in my mind.

# Week 13: 2020-06-13 #
* Thankful that I was able to present my article summary last Thursday. I hope that my classmates learned something.
* I also learned interesting stuff from my classmates as well.
* I am happy with my model for the project. I also was able to experiment with the English letters as well.
* I hope that I will be able to finish my recording this weekend and be able to submit my project a little earlier.


# Article Summaries #

* Kim, Y. (2014). Convolutional Neural Networks for Sentence Classification.
      * This paper focuses on the utilization of CNN on top of pre-trained word vectors for sentence classification.
      * CNN with little hyperparameter tuning and only using static vectors already achieves excellent results on multiple benchmarks.
      * The author proposes an architectural modification wherein task-specific vectors learned via fine-tuning will also be incorporated in the CNN.
      * This approach has improved the state-of-the-art on 4 out of 7 benchmark NLP tasks, including sentiment analysis and question classification.