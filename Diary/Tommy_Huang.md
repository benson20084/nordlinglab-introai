This diary file is written by Tommy Huang C54086204 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-03-15 #

* When reading those successful examples that AIs are applied well, many of them surprised me because I didn't know that AI could be used in those fields and outperform humans like that.
* I learned about how people detect many diseases at once by reading the paper.
* It alerts me that I should study English and Biology more hardly because I got some problems reading papers on the internet.
* I think it would be more interesting for me if the course is not taken online, but nobody can stop the virus, isn't it?

# 2019-03-22 #

* The article is long but interesiting. It guides me to think about lots of questions that I have never met before.
* It makes me understand more about the definition of AI, and get some trivia about it.
* It took me tens of thousands of times looking up the dictionary to read the whole article. My English sucks! :(

# 2019-03-29 #

* I saw one classmate post a question about the task of this week, and I have the same problem ,too.
* However, TA didn't answer the question, so I googled the topic of this week's task myself, and learned some functions of Python.
* The python "help function" is used to display the documentation of modules, functions, classes, keywords etc.
* "Comments" starts with a #, and Python will ignore them.
* "Printing" is to print a message onto the screen.
* Python supports the usual logical conditions from mathematics. They can be used in several ways, most commonly in "if statements" and loops.

# 2019-04-05 #

* A "variable" is essentially a place where we can store the value of something for processing later on.
* You can define "functions" and use them to do some tasks, for example, math programs, in python.
* There are multiple ways to create files in Python, but the cleanest way to do this is by using the "with" keyword.
* There are different modes that you can open a file with.
* "r" is for reading only, "w" is for overwriting the file, "a" is for appending information to a file, and "r+" is for both reading and writing.
* When you open a file you can actually use it as an iterator and iterate over it in the same manner.
* Errors detected during execution are called exceptions and are not unconditionally fatal.
* When errors are detected, you can see what are the exceptions and discriptive messages about them.

# 2019-04-12 #

* As I am majoring in biology, the way that maching learing works is easy to understand, but the complexity of brains is still really incredible.
* After understanding how machine learning works, I think I have to and want to learn more about human's neural network so that I can make more connections between the two fields and my own ideas.
* It takes so much effort to explain how a thing that is similar to and easier than our neural network works! Think about our brains, how hard would it be if we want to figure all their secrets out?

# 2019-04-19 #
* The materials we learn from this week is like applying the concept we learned last week and writing it into codes.
* There are so many terms that make the coding difficult for me to understand.
* We turn visual images into pictures, and then turn pictures into 0 and 1, making them capable of being read by the machine, cool!
