This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown. Note that you should write the diaries for all weeks in the same file.

This diary file is written by Martin Wu E1000000 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* I'm excited about this course but with some worries in the meantime.
* AI lives everywhere in our daily live. 
  For example: agriculture 、mediacal assistance or phtography improvement.
  
# 2020-03-25 #

* Today I have figured out what's difference between "GPU" and "CPU"
* The GPU handles the visual elements and the CPU handles calculation and logic.
* I read a paper about the application of ai in material science. 

# 2020-04-05 #

* I have started learning for python with books and courses on the Internet
* l learned how to use if...else.. and 'while' syntax.
* With these simple coding,I can easily convert my height and weight into my BMI.
* Next step,I'll try my best to learn using python to analyze data and insert mathematics tools.
* And I hope more course video can be uploaded ASAP! I'm extremely worried about myself not catching up with others. 


# 2020-04-12 #
* I am still unsure, how this course will proceed.  I need more thorough guidance on what we are supposed to do.
* But today I found a website that professor mentioned in the discussion room. 
* I followed the instruction of python from that website and I am happy enough to find some good courses on youtube!
* I have searched some introduction about Tensor flow and visited their website for more information.

# 2020-04-19 #
* I take the synchronous lecture this Wednesday but unfortunately I did not check in in time.... 
* I keep learning and practicing python from youtube channel.
* I hope professor can annouce what final group project and final exam will be like as soon as possible,so that I can be well prepared.

# 2020-04-26 #
* This time I take the synchronous lecture exactly on time so was so happy that I did not miss anything.  
* Condition statements,variables , loops and functions are really useful in python,and there are many interesting applications  with them.
* I keep learning and practicing python from youtube channel.

# 2020-05-02 #
* Watch python video lecture on youtube.
* To be honest,I pay little attention on this course recently because I had to deal with many exam and reports.
* I'm intrigued to know something about group project.And I'm really afraid that I don't have enough ability to help our group.

# 2020-05-10 #
* A new mission about group project
* Tonight I have found out so many information about AI for my another course's report. 

# 2020-05-17 #
* I get my group members together to deal with group project.
* Just keep going to catch up lectures that I haven't learned.
* I'm looking forward to physical class.

# 2020-05-22 #
* The first physical class was great,but I was too shy to say something.
* group project working.

# 2020–05-31 #
* Group working 
# 2020-06-03 # 
* For my dreaming future work,I  prefer a more creative work,so I will lke to be the research and development in the company.
* In today's course,we discuss the development between the eletrical cars and oil combustion engine cars.
* Maybe in the near future years,we"ll choose eletrical cars because it cost less.
# 2020-06-14 #
* yeah! Finally,we have already finished group project.
* But worry still A lot for final exam.