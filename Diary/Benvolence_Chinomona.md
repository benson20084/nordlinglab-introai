This diary file is written by Benvolence Chinomona N18077021 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-19 #

* Read the success stories of AI in breast cancer screening. 
* Article: International evaluation of an AI system for breast cancer screening
* Screening mammography can identify breast cancer at earlier stages when treatment can be more successful1
* AI(biopsy) was developed using UK data and performance of the AI system was compared to that of six independent radiologists using a subset of the US test set
* The AI system exceeded the average performance of radiologists by a significant margin (change in area under curve (ΔAUC) = +0.115, 95% CI 0.055, 0.175; P = 0.0002)
* Results shows that the sensitivity advantage of the AI system is concentrated on the identification of invasive cancers (e.g.invasive lobular or ductal carcinoma) rather than in situ cancer (e.g. ductal carcinoma in situ)

# 2020-03-26 #
* The defition of AI depends on the aplication but we can use the teriminologies autonomy and autonomy and adaptivity as key concepts for explaining AI
* Autonomy is described as the ability to perform tasks in complex environments without constant guidance by a user and adaptivity as ability to improve performance by learning from experience.
* The AI taxonomy includes machine learning, which also has its subfields such as data science, deep learning, roboticc etc.
* Until now, the term General AI is applicable only in science ficton and in engineering the term Narrow AI is used, which shows there are limitations to task the machines can handle.
# 2020-04-02 #
* Article review: Vibration Analysis of Gear Box Signal using deep Neural network
* Vibration signal where collected from the gearbox and 62 data sets are used to train and test the model with 256 features from time and frequency domain selected as input parameter for the deep neural networks.
* Statistical features used are mean, standard deviation, kurtosis and skewness. Four deep neural network (DNN) classifiers used are Restricted Boltzmann Machine (RBM), Deep Boltzmann Machine (DBM), Deep Belief Networks (DBN) and Stack Auto-encorders (SAE).
* The results indicated that the DNN based classifiers are able to avoid falling into "aparant local minima or plateaus"and are reliable and robust for gear box fault diagnosis.
  
# 2020-04-09 #
* lnstalation and basics python function using Google Colab
* Learn how to use tensorflow with Keras - Sequential model with different activation function 
* Since am familiar with LSTM model using Matlab toolbox, l have learnt coding using python using LSTM model.
* ln  addition,  we can concatinate CNN and LSTM for training text and images datasets using tensorflow 2.0.
# 2020-04-16 #
* The video visualisation of an artificial neural network provides a clear and simple understanding of machine learning
* l realized that high activation does not necessary give better output and the meaning mathematical formulation and function of the sigmoid is to have the activation function between 0 and 1.
* Definition of Learning - finding the valide weights and biases to perfom the given task
* Machine learning basics - learn how to solve classification task with missing inputs (learn a probability distribution over all the relevant variables, then solve the classication task by marginalizing out the missing variables.)
* Also learnt the difference between unsupervised and supervised learning algorithm
# 2020-04-23 #
* This week l watch the videos 1. How Convolutional Neural Networks work which gave the explanation of how map the features to get the image. 2. Gradient descent, how neural networks learn|Deep learning, chapter 2.
* lmportant points from Chapter 9.1-9.3 are: When using convolutions over more than one axis at a time, for example using a two-dimensional image as input, mostly a two-dimensional kernel is used.
* Convolution leverages three important ideas that can help improve a machine learning system: sparse interactions,parameter sharing and equivariant representations. 
* The sparse interactions is accomplished by making the kernel smaller than the input,parameter sharing means we learn only one set of parameters and equivariant means if the input changes, the output changes in the same way. 
* Pooling helps to make the representation invariant to small translations of input and pooling over spatial regions produces invariance to translation, but pooling the outputs of separately parametrized convolutions, the features can learn which transformations to become invariant.
# 2020-04-30 #
* This week l have learnt the application of Stochastic gradient descent and Backpropagation following the example of recognizing handwritten digits by Michael Nielsen.
* The gradient descent algorithm helps to find a set of weights and biases which make the cost as small as possible i.e. mininimizing the cost funtion (C(w,b)). 
* The optimization algorithm may not be guaranteed to arrive at even alocal minimum in a reasonable amount of time, but it often finds a very low valueof the cost function quickly enough to be useful.
* Backpropagation allows fast computation of cost assumptions i.e. the cost function can be written as an average and the cost can be written as a function of the outputs from the neural network.
# 2020-05-07 #
* This week l did indepth review of some articles in Benchmarks.AI e.g Recurrent Convolutional Neural Network (RCNN) for Object Recognition.
* Using RCNN structure enabled the units to be modulated by other units in the same layer, which enhanced the capability of the CNN to capture statistical regularities in the context of the object. 
* The recurrent connections increased the depth of the original CNN while kept the number of parameters constant by weight sharing between layers. 
# 2020-05-14 #
* Attended the lecture and learnt the ethics and the danger of algorithm bias and how we can improve the algorithm to detect fake videos.
* This week l also started working on the project, first we selected a group leader. 
* The group assigning roles (e.g study on effect of learning rate, dropout, weights etc )for the final project.
# 2020-05-21 #
* Writting the program for image recognition using google colab
* The initial results shows 90 % training and 88 % testing accuracy ising MNIST dataset.
* In addition, l resumed to read the araining of Deep Neural Networks using supervised learning.
# 2020-05-28 #
* Continued to work on the group project(Image recognition) 
* Study of the article on Deep Neural Networks using supervised learning.
* Article: Accelerating Training if Deep Neural Network with a Standardized Loss
# Article summary #
* The standardization loss can significantly solve the problem of layer normalization and group normalization. The formulation is flexible and robust across different batch sizes and accelerates learning on the primary training objective without need to re-tune hyperparameters. 
* This loss measures how far a given distribution is from standardized representation with the loss applied during training by adding to the primary objective (e.g cross ethropy) with a selected weight.
* Across 1000 training steps, the standardization loss reliably accelerated training on the primary classification objective for both training and evaluation data but not faster than batch normalization.
* When the standardization loss is over the four orders of magnitude there is increased cross validtaion perfomance. However beyond a particular weighting the predictive perfomance will collapse.
* Even though batch normalization performs better, it requires the user to maintain moving averages at test  and performs poorly on small batch size. Hence the standardization loss is simple and applicable to several architectures.
