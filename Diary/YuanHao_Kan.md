This diary file is written by YuanHao Kan C64054013 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #

* Though due to the pandemic there is no physical course, I am interested in learning AI.
* AI is really useful for perple in multiple areas.
* AI is also powerful in banking.
* AI banking can be used from front office, middle office to back office.
* The most important is that it may help us reduce bias in credit underwriting.

# 2020-03-25 #

* There is not a clear definition of AI.
* AI has wide applications on our life.
* Some AI methods can focus on some particular points but not the general ones.
* People may be misled by the words learning, understanding, and intelligence.
* AI is different from people so it can do somethings hard to us but cannot do somethings easy for us.

# 2020-04-01 #

* I watched the video "But what is a Neural Network?"
* It surprised me that the recognition of digit images is so complicated.
* Hidden layers might seem like a mystery, but it could be found out.
* ReLU might be more practical than Sigmoid.
* I also read article about machine learning.
* There are several different tasks for machine learning.
* Each performance measure follows a specific task.
* Unsupervised and supervised learning each has its strength in specific field of learning.
* Linear regression is one of the easiest learning.
* High capacity results in overfitting while low capacity results in underfitting.
* There is no free lunch, even for machine learning.
* When the dataset is too small, cross-validation can help.

# 2020-04-15 #

* This is the first live online course.
* Nice to meet Dr. Nordling.
* The audio and video signal is not good.
* I am looking forward to taking the coming classes.

# 2020-04-19 #
* I did the python exercise on colab.
* I have learned a bit python before, but I was still happy to review it.
* Condition statements, loops, variables and functions are really useful in python.

# 2020-04-23 #
* This is the second online live course.
* Dr. Nordling answered us many questions.
* I am really excited about studying machine learning.
* I also watched the video 'TensorFlow and Deep Learning without a PhD' by Martin G�rner.
* TensorFlow is a good tool for studying machine learning and we can use it in the cloud.
* With the help by Relu function to activate and con2v function to shoot neurons in the fully connected layer, it achieveed 99% accuracy in digit recognization.
* I read the write-up and those codes, but I felt it was really a bit too hard for me as a student without relevant background.

# 2020-04-29 #
* It is the third online live course.
* I watched the video "How Convolutional Neural Networks work" by Brandon Rohrer.
* The video made me know more clear about the exact process of CNNs frm convolution, ReLU, to pooling.
* It could be deep stacking by repeating the process for several times.
* A fully connected layer could give us an answer because every value got a vote.
* I also read the textbook from 9.1-9.3.
* Sparse interactions help lower the requirements of parameters and operations.
* Parameter sharing reduces the storage requirements further.
* From convolution to detector stage, and then pooling function.

# 2020-05-06 #
* It is the fourth online live course.
* I watched the video "Gradient descent, how neural networks learn" by 3blue1brown.
* The video Neural Network Function in the example has 784 inputs(pixels) and 10 outputs(1-10) and 13002 parameters(weights/biases).
* The Cost Function has 13002 inputs(weights/biases) and only 1 output(the cost) and many parameters(training examples).
* It is hard to find the global minima but it is relatively easy to find the local minima.
* Also, the video "What is backpropagation really doing?"
* Backpropagation helps us increase the activation by increasing the biases, weights, or changing the activations.
* Stochastic Gradient Descent creates mini-batches to speed up the computation process.
* Then, I watched the video "Backpropagation Calculus."
* Calculus helps backpropagation works by finding the valley of the cost and the sensitivity of each vector.
* Even when each layer has multiple neurons, it is still capable to sum it up and get the model.
* Moreover, I read the 5.9 of textbook "Deep Learning," "Stochastic Gradient Descent(SGD)."
* SGD helps reducing the computational cost on a fixed model size.
* Furthermore, I read the article "Using neural nets to recognize handwritten digits."
* Sigmoid neuron and perceptron are two kinds of artificial neuron while sigmoid one is binary and perceptron can be any number between 0 and 1.
* The bigger the bias, the easier the output to fire.
* When using sigmoid neuron, we can moderately adjust those vectors without hugely changing the outputs.
* If the z of sigmoid neuron is too big, its output will be 1; if it is too negative, its output will be 0.
* Without loops, it is a feedforward neural network.
* MNIST data set is consisted of tens of thousands of handwritten digits.
* We could try to lower the cost.
* Calculus won't work when there are too many variables.
* Mini-batch is a good way to reduce computation cost.
* By adjusting number of epoches, the learning rate, layer number and so on, we can improve the algorithm.

# 2020-05-13 #
* It is the fifth online live course.
* I have contacted my group members.
* Data science is the combination of computer sciences, math & statistics and domains/business knowledge.
* Deep learning is one part of machine learning, while machine learning is part of AI.
* I watched the Ted speech "How we can build AI to help humans, not hurt us" by Margaret Mitchell.
* AI is a function but it could benefit or harm us based on how we use it.
* When training the AI dataset, we should avoid the existence of prejudice and stereotyping and we have to define our goals and strategies.
* I watched the video "AI "Stop Button" Problem - Computerphile."
* It talked about the probable problems of artificial general intelligence(AGI).
* I believe that to solve those problems, we can refer to the Asimov' "three laws of robotics."

# 2020-05-20 #
* It is the first physical class and nice to meet the teacher.
* It is good to discuss those AI ethics problems.

# 2020-05-27 #
* Our group had a discussion about the project.

# 2020-06-03 #
* My dream job 10 years from now is to be a researcher in the biomedical field.
* I hope to use cutting-edge technologies to do the research and benefit the society and people.
* I also hope AI and automation can help me reduce most of the manual and repetitive work.
* The work security I prefer is entrepreneur because I can be the boss and I can hire people to fulfill my ideal in a bigger scale basis.
* My dream work will be genome sequencing automation(already happening), and robotics in laboratory.
* Digitalisation is the big force for automation.
* Electrical combined with self-driving vehicles are going to change the car industry dramatically.
* It is not surprising that agriculture and manufacturing jobs are the most risky ones to be losed due to the high automation in these industries.
* This is our group project youtube link: https://youtu.be/pCMnYV3iIJs

# 2020-06-10 #
* No course this week.