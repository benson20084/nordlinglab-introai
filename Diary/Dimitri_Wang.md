* Dimitri Wang student number F44061018 03/13 Diary
* Today we try to code Python by ourselves.
* I have succefully modified a lot of exercises.
* On top of that, because I learned C++ programming language before, I compare the differences
* in syntex and the similarities in logics between these two language.
* I found that there are no function which is similar to "help" function in C++.
* 03/27 Diary
* Today I got the concept of "model".
* I know what "All Models Are False" means.
* I dissucuss about the featuring properties of model with my classmates.
* I can't find my previous diaries, they disappeared .
* 03/20 Diary. Previous diaries can be found elsewhere, today I understand how to submit my diary properly. 
* (I have created my diary before deadline every week, 
* but now I don't know where are them, maybe in other files or folders.) 
* Today I got the main concept of neural network.
* Neural network are an imatation of human brain, the structure of it is similar to human brain.
* But the procedures of processing data are achieved by mathematical or statistical approches.
* Neural network, simply put, can recognise things. With further advance, it can predict things.
* Neural network is very powerful when it comes to extremly great amount of data.
* 04/10 Diary
* Today I learned how the computer find the local minimum of the loss function.
* I got the concept of 'filtering'.
* Now I know the techniques such as Max Pooling, Average Pooling and Data Augmentation, to help us find what we want.
* After today's class, espacially the introduce of CNN, I find that there are many mathmatical mechanisms behind
* each judgement of AI.
* 04/24 Diary
* Today I practice to pragramme in Python.
* And I attempted to finish all Tensorflow exercises. 
* Initially, I don't really understand what I should do in today's class, so I opened the PPT file and review some
* knowledges related to Tensorflow.For example, what is Tensorflow, how can we use it, etc.
* 05/08 Diary
* I think today's class is a rethink. After obtaining some techniques, we look back on our class objective, or our
* initial purpose.
* The "Smart Phone Example" demonstrate us the concept of using something without thoroughly understanding the 
* mechanisium behind it.
* So based on that concept and owing to the limited time available for this course, we now focous on main concepts
* and get the whole picture of AI, instead of digging into each detail contained in each subtitle.
* 06/13 Diary
* Today i and my club members discuss about our final report, and we trained several models. We got a 92% accuracy.
* 06/19 Diary
* Today we finished our final report. Although we did it eagerly, I still learned a lot from it. 
* 05/22 Diary
* Firstly, I don't know why last week's diary disappeared...
* I think the algorithm bias is to some degree inevitable, what we can do is just increase the reliability of our
* recognising system.
* Consequently, at least in the near future, human is not dispansible. For example, we still need humain pilot
* for commercial aircraft.
* But I think the idea "detect the bias error", which is mentioned the the TED Talk,
* is a breaking concept in preventing algorithm bias. It is an active way to solve the problem.

* 05/15 Diary
* Actually I didn't attend today's class because I am sick, it was very uncimfortable. I apologise about that.
* However, I ask my friend about today's class and looking other classmates' diary, so I roughly know the main content
* of today's class.
* I think the important thing is not extend things that already exist, instead, only by changing the way we think,
* considering an exsiting problem from a whole new prospect, will make a breakthrough. For example, instead of continously
* ameliorating a reciprocal aircraft engine, we develope a whole new technology--jet engine, instead of persistently relying
* on fossil fuel, we adapt a totally different propulsion system-- ionc motor aircaft, or more commonly seen, 
* fuel cell for cars. I think AI technology is definitly a breakthough like those mentioned above, it is of way importance
* that AI provide us with a totally different, a brand-new approache to reach a conventional goal.

* 04/17 Diary
* Today I know what is Tensorflow, and the basic programming logic behind it.
* I know that for the time being, our database structure can just recognise digits.
* So we have to expand our database in the future. By doing so ,the machine learning could be more powerful, 
* or it could be applied to much more fields.