This diary is written by Bivas Panigrahi (N18047068) for the course of Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists. 

It contains the summary of todays lecture on Introduction to Python.

# 2019-03-06 #

* Content of today's course is intresting.
* I have learned the basics of Python.
* I would like to gather more knowldge on python towards its usage in AI.
* I am curious, why python as a language is being prefereed over other language towards learning AI?
------------------------------------
This diary file is written by Bivas Panigrahi N18047068 in the "class-4: Python practice" for the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-03-13 #

* The lecture was great.
* Python is fast although not easy compared to MATLAB.
* An Intresting link: https://www.learnpython.org/
* I wish to have some lecture where it can be elaborated how to use PYTHON offline.
* May be some basics how to program AI will create more interest.
--------------------------------------
This diary file is written by Bivas Panigrahi N18047068 in the "class-5: Introduction to Neural Networks" for the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-03-20 #
* The video from 3Blue 1 Brown is great.
* Got the basics of how neural network works.
* I found the concept CNN trivial.
* Another Introduction to NN from Welch Lab found to be intresting.https://www.youtube.com/watch?v=bxe2T-V8XRs&list=PLiaHhY2iBX9hdHaRr6b7XevZtgZRa1PoU
----------------------------------------
This diary file is written by Bivas Panigrahi N18047068 in the "class-6: Gradient descent and Backpropagation" for the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-03-27 #
* Underst00d how the back propagation and gradient discent works.
* Understand why all the models are wrong.
* nedd trial and error method decide ur model for specific task.
* I found this content to be intresting.https://www.bogotobogo.com/python/scikit-learn/Artificial-Neural-Network-ANN-1-Introduction.php


----------------------------------------
This diary file is written by Bivas Panigrahi N18047068 in the "class-7: Convolutional Neural Networks" for the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-04-10 #
* Get knowldge of Stochastic Gradient desecnt.
* Importance of DATA, gradient descent, trial & error.
* Can CNN be used for other applications except image recognisation.
* Pooling layers

----------------------------------------
This diary file is written by Bivas Panigrahi N18047068 in the "class-8: Introduction to Tensor flow" for the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-04-17 #
* Basics of tensor flow
* Tensor flow can only read numbers.
* MNIST dat sets
* would like to learn more regarding transfer learning
* with last layer modification, we can use it to recognise other things.

----------------------------------------
This diary file is written by Bivas Panigrahi N18047068 in the "class-9: TensorFlow practice" for the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2019-04-24 #
* Practise tensor flow with MNIST dat sets.
* Would like to learn more regarding the fundmanetals of tensor flow.
* What is KERAS, I would like to knoe more?
* Would like to know, if we can use MATLAB to do the aforementioned.