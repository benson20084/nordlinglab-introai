This diary file is written by Jack Lai B34064061 in the course Introduction to Artificial Intelligence and Deep Learning for Engineers and Scientists.

# 2020-03-18 #
* search for many A.I. applications
* upload my ppt about A.I. successful story
* visit many classmates' works
* A.I. is very useful in many area
# 2020-03-25 #
* read What is AI, Deep Learning, and Machine Learning?
* with the help of A.I. Humans move into a supervisory role
* Key terminology of A.I. are Autonomy and Adaptivity
* AI is a collection of concepts, problems, and methods for solving them
* intelligent behavior of the system is fundamentally different from actually being intelligent
# 2020-04-01 #
* download Python
* download Visual Studio Code
* learn how to use Python and VSCode
* practice some code programming
# 2020-04-08 #
* learn about neural networks and modelling from the video
* download some code from GitHub under the video and run
* read deep learning textbook from MIT
* machine can help to improve many work
* there are many learning algorithms with different properties
# 2020-04-15 #
* attend my first synchronous online lecture
* try to install tensorflow but fail
* slove the problem by using Anaconda3
* spending long time waiting for model training
* do tensorflow exercise and observe the results
# 2020-04-22 #
* try to run the code with GPU in my computer
* solve the version problem of tensorflow-gpu
* finish "TensorFlow and Deep Learning without a PhD Part 1 video"
* learn some interesting concepts of deep learning
# 2020-04-29 #
* finish "How Convolutional Neural Networks work" by Brandon Rohrer"
* Read Ch. 9.1-9.3 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
* learn the concepts of filtering, convolution, pooling, normalisation
* borrow some machine learning books from library and read
# 2020-05-06 #
* watch  "Gradient descent, how neural networks learn | Chapter 2" by 3blue1brown based on the MNIST example in Neural Networks and Deep Learning by Michael Nielsen
* watch  "What is backpropagation really doing? | Chapter 3" by 3blue1brown.
* watch "Backpropagation calculus | Appendix to deep learning chapter 3" by 3blue1brown.
* understand the basic concept of gradient descent,backpropagation and backpropagation calculus theorem
# 2020-05-13 #
* Read Ch. 5.9 in Goodfellow, I., Bengio, Y. & Courville, A., 2016. Deep learning
* Read "Using neural nets to recognize handwritten digits" in Neural Networks and Deep Learning by Michael Nielsen.
* contact with my group members
# 2020-05-20 #
* watch How we can build AI to help humans, not hurt us
* watch How I'm fighting bias in algorithms
* watch Fake videos of real people -- and how to spot them
* watch AI "Stop Button" Problem - Computerphile (en)
* watch Stop Button Solution? - Computerphile (none)
# 2020-05-27 #
* I think automation is unavoidable in many kinds of job,many people may loss their jobs because of replacing by machine.But their are also new kinds of jobs  be created and some jobs couldn't be replace,so we don't have to worry too much,you just have to prepare yourself and catch the opportunity 
* discuss with my teammate
* read some A.I. articles
## Article summary ##
* A current challenge in artificial intelligence is to design agents that can adapt rapidly to new tasks by leveraging knowledge acquired through previous experience with related activities
* Deep meta-RL involves a combination of three ingredients: (1) Use of a deep RL algorithm to train a recurrent neural network, (2) a training set that includes a series of interrelated tasks, (3) network input that includes the action selected and reward received in the previous time interval
* The learned algorithm builds in domain-appropriate biases, which can allow it to operate with greater efficiency than a general-purpose algorithm.
# 2020-06-03 
## question on class ##
My dream job 10 years from now is robot scientist
build a robot that help people to work 
and the robot wouldn't hurt people
I prefer to be a Entrepreneur,create my own company

* some of teamates quit
* train model by myself
# 2020-06-10 #
* train model by myself
* record the video for project