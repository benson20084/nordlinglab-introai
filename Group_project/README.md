Your [Jupyter notebook](https://jupyter.org/) with both the training and prediction code, as well as the model specification and final model (saved in HDF5 format), need to be commited to this folder before the last lecture. Please name your files MNIST_Error%_FirstName_FamilyName after the group leader.


# How to download your trained mode?

1. Follow the example in this screenshot
![](https://i.imgur.com/HqtPQA0.png)  

# How to upload the files to the group project folder?  
 
Either you can use a git GUI, such as [Sourcetree](https://www.sourcetreeapp.com/), or [git from the command line/terminal/shell](https://www.atlassian.com/git/tutorials/install-git). The following instructions are for using git in a Bash shell.

1. On Mac and Linux git is typically installed, so open a Terminal (bash shell) and type ```git --version``` and press return (enter). If it returned a *git version ...*, then it is installed. On Windows install [git bash](http://opensourcerer.diy.org/challenge/3) and open it (as instructed on the website). 
   (If you want to, you can continue learning from [this tutorial](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud) yourself. The following instructions only cover the minimum needed to upload the files.) 

1. In the Bash shell type (or copy and paste, using right click to do so):  
    
		cd ~ 
        mkdir dirname  
        cd ~/dirname/  
        git config --global user.email "you@example.com"
   
    Change *dirname* to a folder name you want to use for storing your git repositories. Change the *you@example.com* to the email you use to login to Atlassian/Bitbucket and press return (enter).

1. Get the link for cloning the repository as below, paste it in the Bash shell and press return (enter)  
![](https://i.imgur.com/k2txENj.png)  

1. Now look for the folder with the *dirname* you chose earlier in your computer. It should be in your home folder.   

1. In that folder there should now be a *nordlinglab-introai* folder and in it there is the Group_project folder.  
   Copy and paste the files you want to upload into the Group_project folder.  

1. In the Bash shell type ```cd ~/dirname/nordlinglab-introai/Group_project/``` and press return (enter) to go to that folder. (Note that *dirname* should be whatever you chose before)  

1. In the Bash shell type ```git status``` and press return (enter) to see a list including the files you want to upload. Copy and paste the filenames into e.g. a notepad window. For example:

        MNIST_0.45%_Abraham_TAN_Group_6.ipynb  
        MNIST_0.45%_Abraham_TAN_Group_6_MNIST_model.h5

1. Add the command ```git add ``` before each line of the text you copied earlier, then copy these and paste it into the Bash shell and press return (enter) for it to excecute. This adds the selected files to the git repository. (You can paste and run multiple lines at once. Alternatively, you can just do this in the Bash shell one line at a time.) For example:  

        git add MNIST_0.45%_Abraham_TAN_Group_6.ipynb 
        git add MNIST_0.45%_Abraham_TAN_Group_6_MNIST_model.h5  

1. In the Bash shell type ```git commit -m 'Your commit message here'```, change the commit message, and press return (enter) to commit with the commit message you wrote.  

1. Lastly, in the Bash shell type ```git push origin master``` and press return (enter) and it should start uploading. After it is done, wait a few moment before checking that the Bitbucket website updates.  

If you cannot manage to do this, please contact the TA or a classmate to get help. This example is based on one written by Abraham Tan in 2019.