# List of article summaries with links to YouTube videos

Example of how to list an article with links to the original source, presentation, and video

1. [LeCun et al. 2015 Deep Learning](https://doi.org/10.1038/nature14539), [Summary slides by Matin Wu E1000000](https://drive.google.com/file/d/1SpY65QCia-BWZInvf3286ZMfLZyN34YP/view?usp=sharing), [YouTube video by by Matin Wu E1000000](https://youtu.be/-XyF-sJYiDQ).


-------------------------------2020-------------------------------

Presentation on Thu June 11th 13:10-15:00 in 91101

2. [Chen & Gupta; 2015 Webly Supervised Learning of CNN](http://openaccess.thecvf.com/content_iccv_2015/papers/Chen_Webly_Supervised_Learning_ICCV_2015_paper.pdf), [Summary slides by Matin Hodek P06088415](https://drive.google.com/open?id=1lRkRZ00Ugyo_nJVsbXptlYpHyo-3vAFj). Score 92.
4. [Collins et al. 2019 Accelerating Training of Deep Neural Network with a Standardization Loss](https://arxiv.org/pdf/1903.00925.pdf), by Benvolence Chinomona N18077021. Score 90.
5. [Kim 2014 Convolutional Neural Networks for Sentence Classification](https://arxiv.org/pdf/1408.5882.pdf), [Summary Slides by Reinald Adrian Pugoy P78077040](https://drive.google.com/file/d/1GIH-mUaQBphzkxI4Q9zCVBsszXvb-IwK/view?usp=sharing). Score 95.
13. [Supervised learning of universal sentence representations from natural language inference data.](https://arxiv.org/abs/1705.02364), [Summary slides by YuShan, Lin N16090120](https://drive.google.com/open?id=1-07EGRqWnvHiMzEzbwQiC562lRChyjCt). Score 91.
16. [Image similarity using Deep CNN and Curriculum Learning](https://arxiv.org/abs/1709.08761), [Summary slides by Le Thi Ngoc Hanh P78087037](). Score 92.
1. [Lopes, V.; Fazendeiro, P.: 2020 A Hybrid Method for Training Convolutional Neural Networks](https://arxiv.org/pdf/2005.04153v1.pdf), Summary slides by Marc Henneberger N16088440: [Powerpoint Version](https://drive.google.com/file/d/1OT1LOh1tziK_csLU_G54clblzxT06frk/view?usp=sharing), [PDF Version](https://drive.google.com/file/d/1IGJx748bS5BJzkmbqYXWM9RvEAG7Io9q/view?usp=sharing). Score 96.


Presentation on Wed June 17th 13:10-15:00 in A1306

3. [Tremblay et al. 2018 Training Deep Networks with Synthetic Data: Bridging the Reality Gap by Domain Randomization](http://openaccess.thecvf.com/content_cvpr_2018_workshops/papers/w14/Tremblay_Training_Deep_Networks_CVPR_2018_paper.pdf), Elvira Khamenok N16088432, [YouTube video](https://www.youtube.com/watch?v=VyQoqdC30Lw), [Slides](https://drive.google.com/file/d/19vLddAjqtkyu4kRwWgXqL9rWwNzky-CA/view?usp=sharing)
6. [Keqin C.；Kun Z ；2019 Image-enhanced Adaptive Learning Rate Handwritten Vision Processing Algorithm Based on CNN ](https://sci-hub.tw/https://ieeexplore.ieee.org/abstract/document/8868913), Summary slides by Bing Chuen Hu N18084036
7. [Daanouni O.; Cherradi B.; Tmiri A. : 2020 Diabetes Diseases Prediction Using Supervised Machine Learning and Neighbourhood Components Analysis](https://dl.acm.org/doi/abs/10.1145/3386723.3387887), Summary slides by Yasmine Atayi-Guedegbé N16088416
8. [Nitish Srivastava et al. 2014 Dropout: A Simple Way to Prevent Neural Networks from Overfitting](http://www.jmlr.org/papers/volume15/srivastava14a/srivastava14a.pdf?source=post_page---------------------------), [Summary slides by Ting-Ju Chen N18081509](https://drive.google.com/drive/u/1/folders/1YuTnmIgJlIDkuZQTpS7wvcnb9eh-GTbK).
9. [Xishuang Dong, Uboho Victor, and Lijun Qian (2020) Two-path Deep Semi-supervised Learning for Timely Fake News Detection](https://arxiv.org/pdf/2002.00763.pdf), [Summary slides by Si-Hua Chen Q78081016](https://drive.google.com/file/d/1buUnbs4-rmImnfUlemjY9FDYGRf9z8r2/view).
10. [PCNN: Deep Convolutional Networks for Short-term Traffic Congestion Prediction](https://arxiv.org/pdf/2003.07033.pdf), Summary slides by Jeff Carnell N78081111, [YouTube](https://youtu.be/IDwEkfUp4SQ)
11. [CNN-based Approach for Cervical Cancer Classification in Whole-Slide Histopathology Images](https://arxiv.org/pdf/2005.13924.pdf),[Summary slides by Muhammad-Fadhlan Afif](https://drive.google.com/drive/u/1/folders/1YuTnmIgJlIDkuZQTpS7wvcnb9eh-GTbK).
12. [Zahra et al. 2020 Medical Image Segmentation](https://arxiv.org/abs/2005.05218), [Summary slides by Howard, Kuan-Hao Huang N18044010](https://drive.google.com/file/d/1dQfav48PmOoYJ47cFyJTCeeK_PXcuVFV/view?usp=sharing).
14. [MobileNet v2: Inverted Residuals and Linear Bottlenecks](https://arxiv.org/abs/1801.04381), [Summary by Carlo Pastoral N26087048](https://drive.google.com/drive/mobile/folders/1HBzlkQm8hiOoJ_cFX9bFXbISjMYK-RR4/1YuTnmIgJlIDkuZQTpS7wvcnb9eh-GTbK?sort=13&direction=a)
15. [Yi et al. 2020 CLEVRER:COLLISION EVENTS FOR VIDEO REPRESENTATION AND REASONING](https://arxiv.org/abs/1910.01442), [Summary slides by Omar Hernandez E24057031] (https://drive.google.com/file/d/18NHBNyvBD7e6GYbTX3oLjE8egL4lK8f7/view?usp=sharing)[YouTube video by Omar Hernandez E24057031](https://www.youtube.com/watch?v=kV1JqHKAAp0)
16. [Deep Learning using Linear Support Vector Machines](https://arxiv.org/abs/1306.0239), JIAN-HAO,GUO N16080010, [YouTube video](https://www.youtube.com/watch?v=55_MrbPybaU) [Slides](https://drive.google.com/open?id=1lX8wWKuCUrfUMmK32ZfTSTNianFMVsfJ)
