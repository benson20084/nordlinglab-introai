# How to use Bitbucket web commit to create your diary?

1. Read the [example diary](https://bitbucket.org/temn/nordlinglab-introai/src/master/Diary/Martin_Wu.md) and click *Edit*. (If you cannot click *Edit*, then you lack write access and need to follow [this guide](./README_BITBUCKET.md).)![](https://bitbucket.org/temn/nordlinglab-introai/downloads/Edit_example.png)

2. Copy all the text in the [example diary from the editor](https://bitbucket.org/temn/nordlinglab-introai/src/master/Diary/Martin_Wu.md?at=master&mode=edit&spa=0&fileviewer=file-view-default) and click on *Source*.![](https://bitbucket.org/temn/nordlinglab-introai/downloads/Copy_source_example.png)

3. Enter the Diary folder.![](https://i.imgur.com/Qpj5B7U.png)

4. Create your own diary.![](https://i.imgur.com/BStGmlp.png)

5. Change your file name according to the syntax `FirstName_FamilyName.md` and paste the text you copied from the [example diary](https://bitbucket.org/temn/nordlinglab-introai/src/master/Diary/Martin_Wu.md?at=master&mode=edit&spa=0&fileviewer=file-view-default). Please follow the [example diary](https://bitbucket.org/temn/nordlinglab-introai/src/master/Diary/Martin_Wu.md) and use [Markdown Syntax](https://www.markdownguide.org/basic-syntax/). *NOTE: IF YOU DON'T FOLLOW THE FILE NAME SYNTAX EXACTLY, THEN OUR AUTOMATIC SYSTEM FOR COUNTING ATTENDANCE DOES NOT WORK AND YOU GET NO ATTENDANCE SCORE UNLESS YOU ASK THE TA TO CORRECT YOUR FILE NAME.*![](https://i.imgur.com/10dFKv3.png)

6. Click *Commit*.![](https://bitbucket.org/temn/nordlinglab-introai/downloads/Change_filename_commit.png) 

7. Edit the commit message so it briefly summarises the change you made and click *Commit*. 

8. If there is a merge problem you get "We had trouble merging your changes. We stored them on the ...", then *create a pull request*. ![](https://i.imgur.com/GZN8EKx.png)

9. Click *Merge* twice. ![](https://i.imgur.com/R5lEd6x.png)![](https://i.imgur.com/KGQu7is.png)

10. Now you can find your file in the Diary folder. You can modify it by clicking on it and then clicking *Edit*. 

# What to write in your diary?

1. Start by reading the [example diary](https://bitbucket.org/temn/nordlinglab-introai/src/master/Diary/Martin_Wu.md). Note the layout, in particular the heading and the bullet points. Next click the edit button to see the syntax that generated the layout, in particular the use of # to generate headings and * to generate bullet points. Please note that whitespaces are essential to get the right syntax. Copy all the text of the Martin_Wu.md example (Click Edit before you copy) and paste it in your own diary file.
2. Edit your own diary file. Ensure that it contains your full name, your student number, and a diary entry for each previous week. The reason for why I ask you to add an entry also for the first week is so that you hopefully realise that it is hard to remember your thought and thereby gain motivation and see the value of writing in your diary each time you study. Attendance based on the diary is only counted from week three onwards.
3. Follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).
4. Each time you watch a video or study I want you to write down you thoughts in your diary in bullet point format. I would like to see the questions you get while watching the video, the things that surprise you, the things you think are important, because that will help you develop your ability to critically examine new knowledge and learn. (You are welcome to number your bullets if you want.) Please don't write more than three sentences in each bullet point.
5. Note that you are writing the diary to aid in your own learning and the learning of your classmates. I recommend that you make a habit of reading 3 randomly picked diaries every week to learn from your classmates.
6. Note that there may only be one diary file with your name. This one you update week by week.
